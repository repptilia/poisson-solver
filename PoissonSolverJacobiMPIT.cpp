#include "PoissonSolverJacobiMPIT.hpp"

PoissonSolverJacobiMPIT::PoissonSolverJacobiMPIT(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,int rank,int neighborMin,int neighborMax,size_t numThreads):PoissonSolverJacobiMPI(sizeX,sizeY,sizeZ,rank,neighborMin,neighborMax), m_numThreads(numThreads),m_finishedThreads(0){
	pthread_mutex_init( &m_mutex, NULL);
	pthread_cond_init( &m_cond, NULL);
}

void PoissonSolverJacobiMPIT::iterate(){
	m_err=0.0;
	// Send border data through non-blocking signals
	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory shift(and size)
	int shift(m_sizeZ*m_sizeY);
	if(m_neighborMin!=-1){
		MPI_Irecv(m_data,shift,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+shift,shift, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_data+(m_sizeX-1)*shift,shift,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+(m_sizeX-2)*shift,shift ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
	// Meanwhile, launch pthreads to compute interior of domain
	// Create threads
	pthread_t* threads = (pthread_t*)malloc(m_numThreads*sizeof(pthread_t));
	t_data* data = (t_data*)malloc(m_numThreads*sizeof(t_data));
	m_finishedThreads = 0;
	m_blockThreads = true;
	// Set mutex
	pthread_mutex_init(&m_mutex,NULL);
	for(unsigned int t(0);t<m_numThreads;t++){
		data[t].pb = this;
		data[t].rank=t;
		int rc(pthread_create(&threads[t],NULL,&PoissonSolverJacobiMPIT::t_iterate,(void*)&data[t]));
		if(rc){
			std::cout << "Error from pthread_create : " << rc << std::endl;
		}
	}
	// Wait for all the tasks
	MPI_Waitall(l,request,status);
	// Broadcast signal ready
	m_blockThreads = false;
	pthread_cond_broadcast(&m_cond);

	for(unsigned int t(0);t<m_numThreads;t++){
		pthread_join(threads[t],NULL);
	}
	// swap addresses to apply changes
	P_REAL* swap(NULL);
	swap=m_data;
	m_data=m_tmp;
	m_tmp=swap;
}

void PoissonSolverJacobiMPIT::iteratePartial(size_t rank){
	// Divise units where integration is needed
	unsigned int del(0);
	while(del*m_numThreads<m_sizeZ-2){
		del++;
	}
	unsigned int shift(m_sizeY*m_sizeZ);
	// Boundaries for z
	unsigned int min(del*rank+1);
	unsigned int max=(1+del*(rank+1))<m_sizeZ-1?1+del*(rank+1):m_sizeZ-1;
	// To keep locking mutex only once, use partial values;
	P_REAL my_err(0);
	//unsigned int my_nze(0);
	unsigned int b(2*shift+m_sizeZ);
	for(unsigned int i(2);i<m_sizeX-2;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){
			for(unsigned int k(min);k<max;k++){
				m_tmp[b+k]=m_j_ax*(m_data[-shift+b+k]+m_data[shift+b+k])+m_j_ay*(m_data[b-m_sizeZ+k]+m_data[b+m_sizeZ+k])+m_j_az*(m_data[b+k-1]+m_data[b+k+1])-m_j_af*m_func[b+k];
				my_err += pow(m_cX*(m_data[-shift+b+k]+m_data[shift+b+k]-2.0f*m_data[b+k])+m_cY*(m_data[b-m_sizeZ+k]+m_data[b+m_sizeZ+k]-2.0f*m_data[b+k])+m_cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0f*m_data[b+k])-m_func[b+k],2);
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Wait for boundary data
	pthread_mutex_lock(&m_mutex);
	if(m_blockThreads){
		pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);
	// Now that we have data, update boundaries
	b=m_sizeZ;
	unsigned int z((m_sizeX-2)*shift);
	for(unsigned int j(1);j<m_sizeY-1;j++){
		for(unsigned int k(min);k<max;k++){
			m_tmp[shift+b+k]=m_j_ax*(m_data[b+k]+m_data[2*shift+b+k])+m_j_ay*(m_data[shift+b-m_sizeZ+k]+m_data[shift+b+m_sizeZ+k])+m_j_az*(m_data[shift+b+k-1]+m_data[shift+b+k+1])-m_j_af*m_func[shift+b+k];
			m_tmp[z+b+k]=m_j_ax*(m_data[z-shift+b+k]+m_data[z+shift+b+k])+m_j_ay*(m_data[z+b-m_sizeZ+k]+m_data[z+b+m_sizeZ+k])+m_j_az*(m_data[z+b+k-1]+m_data[z+b+k+1])-m_j_af*m_func[z+b+k];
			my_err += pow(m_cX*(m_data[b+k]+m_data[2*shift+b+k]-2.0f*m_data[shift+b+k])+m_cY*(m_data[shift+b-m_sizeZ+k]+m_data[shift+b+m_sizeZ+k]-2.0f*m_data[shift+b+k])+m_cZ*(m_data[shift+b+k-1]+m_data[shift+b+k+1]-2.0f*m_data[shift+b+k])-m_func[shift+b+k],2)
			+pow(m_cX*(m_data[z-shift+b+k]+m_data[z+shift+b+k]-2.0f*m_data[z+b+k])+m_cY*(m_data[z+b-m_sizeZ+k]+m_data[z+b+m_sizeZ+k]-2.0f*m_data[z+b+k])+m_cZ*(m_data[z+b+k-1]+m_data[z+b+k+1]-2.0f*m_data[z+b+k])-m_func[z+b+k],2);
		}
		b+=m_sizeZ;
	}
	// Wait for all threads
	pthread_mutex_lock(&m_mutex);
	m_err+=my_err;
	m_finishedThreads++;
	// On finishing thread, compute error
	if(m_finishedThreads==m_numThreads){
		// Reduce (p,Ap)
		P_REAL gErr(0);
		MPI_Allreduce ( &m_err, &gErr, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
		m_err=gErr;
		// Release all threads
		m_finishedThreads = 0;
	}
	pthread_mutex_unlock(&m_mutex);
}

void* PoissonSolverJacobiMPIT::t_iterate(void* item){
	t_data* data = new t_data();
	data = (t_data *)item;
	data->pb->iteratePartial(data->rank);

	pthread_exit(NULL);
}
