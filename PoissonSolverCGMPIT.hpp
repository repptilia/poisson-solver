#ifndef POISSONSOLVERCGMPIT_HPP
#define POISSONSOLVERCGMPIT_HPP

#include "PoissonSolverCGMPI.hpp"

class PoissonSolverCGMPIT:public PoissonSolverCGMPI{
	public:
		PoissonSolverCGMPIT(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, P_REAL tol, int rank, int neighborMin, int NeighborMax,size_t numThreads);

		void iterate();
		void set();
		// Called in thread
		void iteratePartial(size_t rank);

		// Static method to be called in thread
		static void* t_iterate(void* item);

		~PoissonSolverCGMPIT();

	protected:
		size_t m_numThreads;
		size_t m_finishedThreads;
		// Range of action of thread
		unsigned int m_del;
		// mutex
		pthread_mutex_t m_mutex;
		// condition variable
		pthread_cond_t m_cond;
		// "Globalized" to be worked with in pthreads
		P_REAL m_pAp;
		P_REAL m_alpha;
		P_REAL m_beta;
		P_REAL m_rNew;
		// Bool variable blocking if MPI requires to wait
		bool m_blockThreads;
};

struct t_cg_data{
	PoissonSolverCGMPIT* pb;
	size_t rank;
};

#endif // POISSONSOLVERCGMPIT_HPP
