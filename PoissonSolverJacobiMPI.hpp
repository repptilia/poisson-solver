#ifndef POISSONSOLVERJACOBIMPI_HPP
#define POISSONSOLVERJACOBIMPI_HPP

#include "ProblemMPI.hpp"
#include "PoissonSolverJacobi.hpp"

class PoissonSolverJacobiMPI:public PoissonSolverJacobi, public ProblemMPI{
	public:
		PoissonSolverJacobiMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,int rank,int neighborMin,int neighborMax);

		void iterate();
};

#endif // POISSONSOLVERJACOBIMPI_HPP
