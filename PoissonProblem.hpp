#ifndef POISSONPROBLEM_HPP
#define POISSONPROBLEM_HPP
#include <iostream>
#include <vector>
#include <stdlib.h>	// malloc
#include <cmath>	// fabs

//TODO: add support for long P_REAL

#define _X_MIN 0
#define _X_MAX 1
#define _Y_MIN 2
#define _Y_MAX 3
#define _Z_MIN 4
#define _Z_MAX 5

#ifdef _P_REAL_PRECISION
#define P_REAL	double
#define R	
#else
#define P_REAL	float
#define R	f
#endif

class PoissonProblem{
	public:
		PoissonProblem(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ);
		// Set limits of domain

		void print();
		void printF();
		void printD();

		void xLim(P_REAL min,P_REAL max);
		void yLim(P_REAL min,P_REAL max);
		void zLim(P_REAL min,P_REAL max);

		virtual void setBdry( P_REAL (*bdry)(P_REAL,P_REAL,P_REAL),unsigned int direction);

		virtual void iterate() = 0;

		// Returns the error of iteration
		virtual P_REAL error() = 0;

		virtual void set();
		void setFunc( P_REAL (*func)(P_REAL,P_REAL,P_REAL));

		// Print data into stream
		virtual void printData(std::ostream& stream);


	protected:
		unsigned int m_sizeX;	// Size of mesh in direction X
		unsigned int m_sizeY;	// Size of mesh in direction Y
		unsigned int m_sizeZ;	// Size of mesh in direction Z
		P_REAL* m_data;
		P_REAL* m_func;
		P_REAL m_corner[6];
		P_REAL m_delX;
		P_REAL m_delY;
		P_REAL m_delZ;
		P_REAL m_cX;
		P_REAL m_cY;
		P_REAL m_cZ;

		// Looping size
		size_t m_maxIt;
};


std::ostream& operator<<(std::ostream& out, PoissonProblem& p);

#endif // POISSONPROBLEM_HPP
