#ifndef POISSONSOLVERJACOBICMPI_HPP
#define POISSONSOLVERJACOBICMPI_HPP

#include "PoissonSolverJacobiC.hpp"
#include "ProblemMPI.hpp"

/**
 * Allow for providing default
 * NOTE:
 *  It would be dangerous to go higher, as 8 size in 1 direction results in 512 threads in one block, which is almost the limit of conventional GPUs.
 */
#ifndef BLOCK_SIZE
#define BLOCK_SIZE	8
#endif

class PoissonSolverJacobiCMPI:public PoissonSolverJacobiC, public ProblemMPI{
	public:
		PoissonSolverJacobiCMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, int device,int rank,int neighborMin,int neighborMax);

		void set();
		void iterate();

	protected:
};

#endif // POISSONSOLVERCGCMPI_HPP
