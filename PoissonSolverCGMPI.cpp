#include "PoissonSolverCGMPI.hpp"

PoissonSolverCGMPI::PoissonSolverCGMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, P_REAL tol, int rank, int neighborMin, int neighborMax): PoissonSolverCG(sizeX,sizeY,sizeZ,tol), ProblemMPI(rank,neighborMin,neighborMax){
}

void PoissonSolverCGMPI::set(){
	m_cX = 1.0/m_delX;
	m_cY = 1.0/m_delY;
	m_cZ = 1.0/m_delZ;
	m_normr = 0.0;

	// Compute Ap and (p,Ap) in the same time
	// Send border data through non-blocking signals
	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory sX(and size)
	int sX(m_sizeZ*m_sizeY);
	if(m_neighborMin!=-1){
		MPI_Irecv(m_data,sX,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+sX,sX, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_data+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
	unsigned int b(2*sX+m_sizeZ);
	for(unsigned int i(2);i<m_sizeX-2;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_r[b+k]=m_func[b+k]-m_cX*(m_data[b+k-sX]+m_data[b+k+sX]-2.0*m_data[b+k])
						    -m_cY*(m_data[b+k-m_sizeZ]+m_data[b+k+m_sizeZ]-2.0*m_data[b+k])
						    -m_cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0*m_data[b+k]);
				m_p[b+k]=m_r[b+k];
				m_normr+=m_r[b+k]*m_r[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Wait for all the tasks
	MPI_Waitall(l,request,status);
	// Now that we have data, update boundaries
	b=m_sizeZ;
	unsigned int z((m_sizeX-2)*sX);
	for(unsigned int j(1);j<m_sizeY-1;j++){		
		for(unsigned int k(1);k<m_sizeZ-1;k++){
			m_r[sX+b+k]=m_func[sX+b+k]-m_cX*(m_data[b+k]+m_data[b+k+2*sX]-2.0*m_data[sX+b+k])
					    -m_cY*(m_data[sX+b+k-m_sizeZ]+m_data[sX+b+k+m_sizeZ]-2.0*m_data[sX+b+k])
					    -m_cZ*(m_data[sX+b+k-1]+m_data[sX+b+k+1]-2.0*m_data[sX+b+k]);
			m_p[sX+b+k]=m_r[sX+b+k];
			m_normr+=m_r[sX+b+k]*m_r[sX+b+k];
			m_r[z+b+k]=m_func[z+b+k]-m_cX*(m_data[z+b+k-sX]+m_data[z+b+k+sX]-2.0*m_data[z+b+k])
					    -m_cY*(m_data[z+b+k-m_sizeZ]+m_data[z+b+k+m_sizeZ]-2.0*m_data[z+b+k])
					    -m_cZ*(m_data[z+b+k-1]+m_data[z+b+k+1]-2.0*m_data[z+b+k]);
			m_p[z+b+k]=m_r[z+b+k];
			m_normr+=m_r[z+b+k]*m_r[z+b+k];
		}
		b+=m_sizeZ;
	}
	// Collect global (p,Ap)
	P_REAL gnormr(0.0);
	MPI_Allreduce ( &m_normr, &gnormr, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
	m_normr=gnormr;
}

void PoissonSolverCGMPI::iterate(){
	P_REAL cX(1.0/m_delX);
	P_REAL cY(1.0/m_delY);
	P_REAL cZ(1.0/m_delZ);
	// Compute Ap and (p,Ap) in the same time
	// Send border data through non-blocking signals
	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory sX(and size)
	int sX(m_sizeZ*m_sizeY);
	if(m_neighborMin!=-1){
		MPI_Irecv(m_p,sX,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_p+sX,sX, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_p+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_p+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
	P_REAL pAp(0.0);
	unsigned int b(2*sX+m_sizeZ);
	for(unsigned int i(2);i<m_sizeX-2;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_Ap[b+k]=cX*(m_p[b+k-sX]+m_p[b+k+sX]-2.0*m_p[b+k])
					 +cY*(m_p[b+k-m_sizeZ]+m_p[b+k+m_sizeZ]-2.0*m_p[b+k])
					 +cZ*(m_p[b+k-1]+m_p[b+k+1]-2.0*m_p[b+k]);
				pAp+=m_p[b+k]*m_Ap[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Wait for all the tasks
	MPI_Waitall(l,request,status);
	// Now that we have data, update boundaries
	b=m_sizeZ;
	unsigned int z((m_sizeX-2)*sX);
	for(unsigned int j(1);j<m_sizeY-1;j++){		
		for(unsigned int k(1);k<m_sizeZ-1;k++){
			m_Ap[sX+b+k]=cX*(m_p[b+k]+m_p[b+k+2*sX]-2.0*m_p[sX+b+k])
				 +cY*(m_p[sX+b+k-m_sizeZ]+m_p[sX+b+k+m_sizeZ]-2.0*m_p[sX+b+k])
				 +cZ*(m_p[sX+b+k-1]+m_p[sX+b+k+1]-2.0*m_p[sX+b+k]);
			pAp+=m_p[sX+b+k]*m_Ap[sX+b+k];
			m_Ap[z+b+k]=cX*(m_p[z-sX+b+k]+m_p[z+b+k+sX]-2.0*m_p[z+b+k])
				 +cY*(m_p[z+b+k-m_sizeZ]+m_p[z+b+k+m_sizeZ]-2.0*m_p[z+b+k])
				 +cZ*(m_p[z+b+k-1]+m_p[z+b+k+1]-2.0*m_p[z+b+k]);
			pAp+=m_p[z+b+k]*m_Ap[z+b+k];
		}
		b+=m_sizeZ;
	}
	// Collect global (p,Ap)
	P_REAL gpAp(0.0);
	MPI_Allreduce ( &pAp, &gpAp, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
	// Compute the coefficient alpha
	P_REAL alpha(m_normr/gpAp);
	// Update x,r and compute the new residual norm
	P_REAL rNew(0.0);
	b=sX+m_sizeZ;
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_data[b+k]+=alpha*m_p[b+k];
				m_r[b+k]-=alpha*m_Ap[b+k];
				rNew+=m_r[b+k]*m_r[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	P_REAL grNew(0.0);
	// Collect the global residual
	MPI_Allreduce ( &rNew, &grNew, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );

	// If convergence reached, stop
	if(grNew<m_tol){
		m_normr=grNew;
		return;
	}
	// Update p, residual norm if convergence not reached
	P_REAL beta(grNew/m_normr);
	b=sX+m_sizeZ;
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_p[b+k]=m_r[b+k]+beta*m_p[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	m_normr=grNew;
}
