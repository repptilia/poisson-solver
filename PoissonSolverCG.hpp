#ifndef POISSONSOLVERCG_HPP
#define POISSONSOLVERCG_HPP

#include "PoissonProblem.hpp"
#define _X_MIN 0
#define _X_MAX 1
#define _Y_MIN 2
#define _Y_MAX 3
#define _Z_MIN 4
#define _Z_MAX 5

class PoissonSolverCG:public PoissonProblem{
	public:
		PoissonSolverCG();
		PoissonSolverCG(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,P_REAL tol);

		//void setBdry( P_REAL (*bdry)(P_REAL,P_REAL,P_REAL),unsigned int direction);
		void set();
		void iterate();

		P_REAL error(){return m_normr;}


	protected:
		// Variables for conjugate gradient iteration
		// Define them here so that we don't need to P_REALlocate them at each iteration
		P_REAL* m_r;
		P_REAL* m_p;
		P_REAL* m_Ap;
		P_REAL  m_normr;

		// Constants always reused
		P_REAL m_cX;
		P_REAL m_cY;
		P_REAL m_cZ;

		// Convergence tolerance
		P_REAL m_tol;
};

#endif // POISSONSOLVERCG_HPP


/*
./poisson_serial -s 100 -m CG -v
Solver took -CPU time: 1.4 s
            -Wall time:1.40712 s

*/
