#include "PoissonSolverJacobi.hpp"

PoissonSolverJacobi::PoissonSolverJacobi(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ) : PoissonProblem(sizeX,sizeY,sizeZ){
	m_tmp = (P_REAL*)malloc((size_t)sizeX*(size_t)sizeY*(size_t)sizeZ*sizeof(P_REAL));
}

void PoissonSolverJacobi::set(){
	PoissonProblem::set();
	P_REAL sum(m_delX*m_delX*m_delY*m_delY+m_delY*m_delY*m_delZ*m_delZ+m_delZ*m_delZ*m_delX*m_delX);
	sum *=2.0;
	m_j_ax = m_delY*m_delY*m_delZ*m_delZ/sum;
	m_j_ay = m_delX*m_delX*m_delZ*m_delZ/sum;
	m_j_az = m_delX*m_delX*m_delY*m_delY/sum;
	m_j_af = m_j_ax*m_delX*m_delX;
}

void PoissonSolverJacobi::setBdry( P_REAL (*bdry)(P_REAL,P_REAL,P_REAL),unsigned int direction){
	switch(direction){
		case _X_MIN:
			for(unsigned int j(0);j<m_sizeY;j++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[j*m_sizeZ+k]=bdry(m_corner[_X_MIN],m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]+k*m_delZ);
					m_tmp[j*m_sizeZ+k]=m_data[j*m_sizeZ+k];
				}
			}
			break;
		case _X_MAX:
			for(unsigned int j(0);j<m_sizeY;j++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[((m_sizeX-1)*m_sizeY+j)*m_sizeZ+k]=bdry(m_corner[_X_MAX],m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]+k*m_delZ);
					m_tmp[((m_sizeX-1)*m_sizeY+j)*m_sizeZ+k]=m_data[((m_sizeX-1)*m_sizeY+j)*m_sizeZ+k];
				}
			}
			break;
		case _Y_MIN:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[i*m_sizeY*m_sizeZ+k]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN],m_corner[_Z_MIN]+k*m_delZ);
					m_tmp[i*m_sizeY*m_sizeZ+k]=m_data[i*m_sizeY*m_sizeZ+k];
				}
			}
			break;
		case _Y_MAX:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[i*m_sizeY*m_sizeZ+m_sizeZ*(m_sizeY-1)+k]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MAX],m_corner[_Z_MIN]+k*m_delZ);
					m_tmp[i*m_sizeY*m_sizeZ+m_sizeZ*(m_sizeY-1)+k]=m_data[i*m_sizeY*m_sizeZ+m_sizeZ*(m_sizeY-1)+k];
				}
			}
			break;
		case _Z_MIN:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int j(0);j<m_sizeY;j++){
					m_data[(i*m_sizeY+j)*m_sizeZ]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]);
					m_tmp[(i*m_sizeY+j)*m_sizeZ]=m_data[(i*m_sizeY+j)*m_sizeZ];
				}
			}
			break;
		case _Z_MAX:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int j(0);j<m_sizeY;j++){
					m_data[(i*m_sizeY+j+1)*m_sizeZ-1]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MAX]);
					m_tmp[(i*m_sizeY+j+1)*m_sizeZ-1]=m_data[(i*m_sizeY+j+1)*m_sizeZ-1];
				}
			}
			break;
	}
}

void PoissonSolverJacobi::iterate(){
	const P_REAL cX(1.0/m_delX);
	const P_REAL cY(1.0/m_delY);
	const P_REAL cZ(1.0/m_delZ);
	
	const size_t sX(m_sizeY*m_sizeZ);

	// Other way of seeing things in order to parallelize computation
	// Constant variables for parallelization
	const P_REAL aX(m_j_ax);
	const P_REAL aY(m_j_ay);
	const P_REAL aZ(m_j_az);
	const P_REAL aF(m_j_af);
	// Shift beginning
	P_REAL* m_tmp_sh(m_tmp+(m_sizeY+1)*m_sizeZ+1);
	// Shifted pointers for parallelization
	const P_REAL* d_pX(m_data+m_sizeZ+1);
	const P_REAL* d_mX(m_data+2*sX+m_sizeZ+1);
	const P_REAL* d_pY(m_data+sX+1);
	const P_REAL* d_mY(m_data+sX+2*m_sizeZ+1);
	const P_REAL* d_pZ(m_data+(m_sizeY+1)*m_sizeZ+2);
	const P_REAL* d_mZ(m_data+(m_sizeY+1)*m_sizeZ);
	const P_REAL* func(m_func+(m_sizeY+1)*m_sizeZ+1);


	m_err=0.;
	//Do calculation for everything, including the edges.
	for( size_t i(0); i < m_maxIt; i++) {
		m_tmp_sh[i]=aX*(d_pX[i]+d_mX[i])+aY*(d_pY[i]+d_mY[i])+aZ*(d_pZ[i]+d_mZ[i])-aF*func[i];
	}

	//Reset the edges.
	for(unsigned x = 0; x < m_sizeX; x++) {
		for(unsigned y = 0; y < m_sizeY; y++) {
			m_tmp[x*sX + y*m_sizeZ] = m_data[x*sX + y*m_sizeZ];
			m_tmp[x*sX + y*m_sizeZ + m_sizeZ-1] = m_data[x*sX + y*m_sizeZ + m_sizeZ-1];
			}
		for(unsigned z = 0; z < m_sizeZ; z++) {
			m_tmp[x*sX + z] = m_data[x*sX + z];
			m_tmp[x*sX + (m_sizeY-1)*m_sizeZ + z] = m_data[x*sX + (m_sizeY-1)*m_sizeZ + z];
		}
	}

	// Compute norm of residual
	// NOTE:This is slow,but allows for comparison with conjugate gradient, which is what we are looking for here
	P_REAL tmp(0.0);
	//Do calculation for everything, including the edges.
	unsigned int b(sX+m_sizeZ);
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				tmp=cX*(m_data[-sX+b+k]+m_data[sX+b+k]-2.0f*m_data[b+k])
				   +cY*(m_data[b-m_sizeZ+k]+m_data[b+m_sizeZ+k]-2.0f*m_data[b+k])
				   +cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0f*m_data[b+k])
				   -m_func[b+k];
				m_err+=tmp*tmp;
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}

	// swap addresses to apply changes
	P_REAL* swap(NULL);
	swap=m_data;
	m_data=m_tmp;
	m_tmp=swap;
}

