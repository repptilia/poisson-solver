#ifndef PROBLEMMPI_HPP
#define PROBLEMMPI_HPP

#include <mpi.h>
#include <unistd.h>
#include "PoissonProblem.hpp"

#if _P_REAL_PRECISION == float
#define _MPI_DATATYPE	MPI_FLOAT
#elif _P_REAL_PRECISION == double
#define _MPI_DATATYPE	MPI_DOUBLE
#else
#error MPI_Datatype undefined.
#endif

class ProblemMPI{
	public:
		ProblemMPI(int rank,int neighborMin,int neighborMax);

		void setNeighbor(int neighbor, unsigned int direction);

	protected:
		int m_me;
		int m_neighborMin;
		int m_neighborMax;
};

#endif // PROBLEMMPI_HPP
