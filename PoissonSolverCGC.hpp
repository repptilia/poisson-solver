#ifndef POISSONSOLVERCGC_HPP
#define POISSONSOLVERCGC_HPP

#include "PoissonSolverCG.hpp"
#include "cudaDef.hpp"

class PoissonSolverCGC:public PoissonSolverCG{
	public:
		PoissonSolverCGC(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,P_REAL tol, int device);

		void set();
		void iterate();

		// Print data into stream
		// Needs overloading as the data stays in GPU memory throughout iterations
		virtual void printData(std::ostream& stream);

		~PoissonSolverCGC();
	protected:
		// Structure variables
		dim3 m_blockSize;
		dim3 m_gridSize;
		// CUDA variables
		P_REAL* d_data;
		P_REAL* d_Ap;
		P_REAL* d_r;
		P_REAL* d_p;
		// For sum reductions
		P_REAL* d_reduced;
		// Local version
		P_REAL* m_reduced;
};

#endif // POISSONSOLVERCGC_HPP
