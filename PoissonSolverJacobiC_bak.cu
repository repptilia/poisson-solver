#include "PoissonSolverJacobiC.hpp"

// Computes new data and previous error
__global__ void update(const P_REAL* data, P_REAL* tmp, const P_REAL* f, P_REAL* reduced, const unsigned int sizeX,const unsigned int sizeY,const unsigned int sizeZ, const P_REAL aX, const P_REAL aY, const P_REAL aZ, const P_REAL aF, const P_REAL cX, const P_REAL cY, const P_REAL cZ){
	extern __shared__ P_REAL s_sh[];

	P_REAL* s_d(&s_sh[blockDim.x*blockDim.y*blockDim.z]);

#if _GRID_DIM == DDD
	// Get grid size
	unsigned int gY((sizeY-2)/blockDim.y),gZ((sizeZ-2)/blockDim.z);

	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*blockIdx.y+threadIdx.y+1);
	unsigned int idZ(blockDim.z*blockIdx.z+threadIdx.z+1);
#elif _GRID_DIM == DD
	unsigned int gX((sizeX-2)/blockDim.x),gY((sizeY-2)/blockDim.y);

	unsigned int tt(blockIdx.x%(gX*gY));
	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*(tt+threadIdx.y+1);
	unsigned int idZ(blockDim.z*((blockIdx.x-tt)/(gX*gY)+threadIdx.z+1);
#endif
	//TODO: Check that the index is within range (wrt how the blocks are constructed)
	// Not needed in current fashion as blocks are arranged st its ok

	unsigned int sX(sizeY*sizeZ);
	unsigned int i((idX*sizeY+idY)*sizeZ+idZ);
	unsigned int bi((threadIdx.x*blockDim.y+threadIdx.y)*blockDim.z+threadIdx.z);

// BENCHMARK 5.67s

	// Since we take "shift" into account, block index bi for shared memory is shifted as well.
/*
	s_d[6*bi]=data[i+1];
	s_d[6*bi+1]=data[i-1];
	s_d[6*bi+2]=data[i+sizeZ];
	s_d[6*bi+3]=data[i-sizeZ];
	s_d[6*bi+4]=data[i+sX];
	s_d[6*bi+5]=data[i-sX];*/

	// On thread bi, we write to the shared memory where other threads would use that information.
	// This way, data[i] is only accessed by a specific thread.
	P_REAL dat(data[i]);


	// Lower boundary will be overwritten anyway by error
	// to -z neighbor:
	s_d[6*(bi-blockDim.z*blockDim.y)]=dat;
	// to -y neighbor:
	s_d[6*(bi-blockDim.z)+2]=dat;
	// to -x neighbor:
	s_d[6*(bi-1)+4]=dat;

	// Upper boundary is allocated
	// to +z neighbor:
	s_d[6*(bi+blockDim.z*blockDim.y)+1]=dat;
	// to +y neighbor:
	s_d[6*(bi+blockDim.z)+3]=dat;
	// to +z neighbor:
	s_d[6*(bi+1)+5]=dat;

	__syncthreads();
	// Take care of boundary effects
	if(threadIdx.x==0){
		s_d[6*bi+1]=data[i-sX];
	}
	if(threadIdx.x==blockDim.x-1){
		s_d[6*bi]=data[i+sX];
	}
	if(threadIdx.y==0){
		s_d[6*bi+3]=data[i-sizeZ];
	}
	if(threadIdx.y==blockDim.y-1){
		s_d[6*bi+2]=data[i+sizeZ];
	}
	if(threadIdx.z==0){
		s_d[6*bi+5]=data[i-1];
	}
	if(threadIdx.z==blockDim.z-1){
		s_d[6*bi+4]=data[i+1];
	}

/*
	// Update tmp,error
//UNCOMMENT
	tmp[i]=aX*(data[i-sX]+data[i+sX])
		+aY*(data[i-sizeZ]+data[i+sizeZ])
		+aZ*(data[i-1]+data[i+1])
		-aF*f[i];

	P_REAL vtmp(cX*(data[i-sX]+data[i+sX]-2.0f*data[i])
		+cY*(data[i-sizeZ]+data[i+sizeZ]-2.0f*data[i])
		+cZ*(data[i-1]+data[i+1]-2.0f*data[i])
		-f[i]);
*/
	__syncthreads();

	tmp[i]=aX*(s_d[6*bi]+s_d[6*bi+1])
		+aY*(s_d[6*bi+2]+s_d[6*bi+3])
		+aZ*(s_d[6*bi+4]+s_d[6*bi+5])
		-aF*f[i];

	P_REAL vtmp(cX*(s_d[6*bi]+s_d[6*bi+1])
		   +cY*(s_d[6*bi+2]+s_d[6*bi+3])
		   +cZ*(s_d[6*bi+4]+s_d[6*bi+5])
		   -2.0f*(cX+cY+cZ)*dat-f[i]);

	s_sh[bi] = vtmp*vtmp;

	// Wait for all threads
	__syncthreads();

	// Take advantage of the geometry of the block
	// At each iteration, there are 7 points to consider
	// Use byte-shift to ease division by 2

	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(threadIdx.x < offset && threadIdx.y < offset && threadIdx.z < offset){
			s_sh[bi]+=s_sh[bi+offset]+s_sh[bi+offset*blockDim.z]+s_sh[bi+offset*blockDim.z*blockDim.y]
					+s_sh[bi+offset*(1+blockDim.z)]
					+s_sh[bi+offset*(1+blockDim.z*blockDim.y)]
					+s_sh[bi+offset*blockDim.z*(1+blockDim.y)]
					+s_sh[bi+offset*(1+blockDim.z*(1+blockDim.y))];
		}

		// Wait for all threads
		__syncthreads();
	}

	// Write final result with (0,0,0) thread
	if(threadIdx.x==0 && threadIdx.y==0 && threadIdx.z == 0){
#if _GRID_DIM == DDD
		reduced[(blockIdx.x*gY+blockIdx.y)*gZ+blockIdx.z] = s_sh[0];
#elif _GRID_DIM == DD
		reduced[blockIdx.y*gY*gZ+blockIdx.x]=s_sh[0];
#endif
	}
}

PoissonSolverJacobiC::PoissonSolverJacobiC(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, int device):PoissonSolverJacobi(sizeX,sizeY,sizeZ){

	// Create grids
	m_blockSize = dim3(BLOCK_SIZE,BLOCK_SIZE,BLOCK_SIZE);
#if _GRID_DIM == DDD
	m_gridSize  = dim3((m_sizeX-2)/m_blockSize.x,(m_sizeY-2)/m_blockSize.y,(m_sizeZ-2)/m_blockSize.z);
#elif _GRID_DIM == DD
	m_gridSize = dim3((m_sizeX-2)/m_blockSize.x,(m_sizeY-2)/m_blockSize.y*(m_sizeZ-2)/m_blockSize.z,1);
#endif
	// Completely re-create constructor, as what we need this time is GPU-side variables mostly
	// Set CUDA device
	cudaSetDevice(device);

	// Convert to size_t
	size_t t((size_t)sizeX*(size_t)sizeY*(size_t)sizeZ);

	cudaMalloc((void**)&d_data,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_tmp,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_func,t*sizeof(P_REAL));

	// Initialize memory
	cudaMemset(d_data,0,t*sizeof(P_REAL));
	cudaMemset(d_tmp,0,t*sizeof(P_REAL));
	cudaMemset(d_func,0,t*sizeof(P_REAL));

	m_maxIt = (size_t)sizeZ*((size_t)sizeY*((size_t)sizeX-2)-2)-2;

	size_t grid(m_gridSize.x*m_gridSize.y*m_gridSize.z);
	m_reduced = (P_REAL*)malloc(grid*sizeof(P_REAL));
	cudaMalloc((void**)&d_reduced,grid*sizeof(P_REAL));
	cudaMemset(d_reduced,0,grid*sizeof(P_REAL));
}

void PoissonSolverJacobiC::set(){
	PoissonProblem::set();
	P_REAL sum(m_delX*m_delX*m_delY*m_delY+m_delY*m_delY*m_delZ*m_delZ+m_delZ*m_delZ*m_delX*m_delX);
	sum *=2.0;
	m_j_ax = m_delY*m_delY*m_delZ*m_delZ/sum;
	m_j_ay = m_delX*m_delX*m_delZ*m_delZ/sum;
	m_j_az = m_delX*m_delX*m_delY*m_delY/sum;
	m_j_af = m_j_ax*m_delX*m_delX;

	// Convert to size_t
	size_t t((size_t)m_sizeX*(size_t)m_sizeY*(size_t)m_sizeZ);

	m_cX = 1.0f/m_delX;
	m_cY = 1.0f/m_delY;
	m_cZ = 1.0f/m_delZ;
	m_err = 0.0f;

	// Transfer data to GPU
	cudaMemcpy(d_data, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tmp, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_func, m_func, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
}

void PoissonSolverJacobiC::iterate(){
	// Number of blocks
	size_t numBlocks(m_gridSize.x*m_gridSize.y*m_gridSize.z);
	// Size of blocks
	size_t sizeBlock(m_blockSize.x*m_blockSize.y*m_blockSize.z);

	/**
	 * NOTE: on shared memory allocation
	 *
	 * To fasten computation, we load partially the "shifted" versions of data on the shared memory.
	 * Therefore, we need to allocate memory for:
	 * -d_data +- 1
	 * -d_data +- m_sizeZ
 	 * -d_data +- m_sizeZ*m_sizeY
	 *
	 * This suggests an allocation of 6*sizeBlock for the whole array
	 * +1 for boundary on top
	 * +1 for error = 7
	 **/

	// Compute on the GPU
	update<<<m_gridSize,m_blockSize,8*sizeBlock*sizeof(P_REAL)>>>(d_data,d_tmp,d_func,d_reduced,m_sizeX,m_sizeY,m_sizeZ,m_j_ax,m_j_ay,m_j_az,m_j_af,m_cX,m_cY,m_cZ);

	// Pull back error
	cudaMemcpy(m_reduced, d_reduced, numBlocks*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	m_err = 0.0f;
	for(unsigned int i(0);i<numBlocks;i++){
		m_err+=m_reduced[i];
	}

	// Swap memories
	P_REAL* swap(NULL);
	swap=d_data;
	d_data=d_tmp;
	d_tmp=swap;
}


void PoissonSolverJacobiC::printData(std::ostream& stream){
	// Pull data back
	cudaMemcpy(m_data, d_data, m_sizeX*m_sizeY*m_sizeZ*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				stream << m_data[b+k] << " ";
			}
			stream << std::endl;
			b+=m_sizeZ;
		}
		stream << std::endl;
	}
}

PoissonSolverJacobiC::~PoissonSolverJacobiC(){
	cudaFree(d_data);
	cudaFree(d_tmp);
	cudaFree(d_func);
	cudaFree(d_reduced);
}

