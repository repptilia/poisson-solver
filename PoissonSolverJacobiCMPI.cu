#include "PoissonSolverJacobiCMPI.hpp"

// Computes new data and previous error
__global__ void m_updateInterior(P_REAL* data, P_REAL* tmp, P_REAL* f, P_REAL* reduced, const unsigned int sizeX,const unsigned int sizeY,const unsigned int sizeZ, const P_REAL aX, const P_REAL aY, const P_REAL aZ, const P_REAL aF, const P_REAL cX, const P_REAL cY, const P_REAL cZ){

	// Global ID
#if _GRID_DIM == DDD
	int idX(blockDim.x*blockIdx.x+threadIdx.x+blockDim.x/2);
	int idY(blockDim.y*blockIdx.y+threadIdx.y);
	int idZ(blockDim.z*blockIdx.z+threadIdx.z);
#elif _GRID_DIM == DD
	int tt(blockIdx.x%(gridDim.x*gridDim.y));
	int idX(blockDim.x*blockIdx.x+threadIdx.x+blockDim.x/2);
	int idY(blockDim.y*(tt+threadIdx.y);
	int idZ(blockDim.z*((blockIdx.x-tt)/(gridDim.x*gridDim.y)+threadIdx.z);
#endif

	// Local ID
	int lidX(threadIdx.x);
	int lidY(threadIdx.y);
	int lidZ(threadIdx.z);


	const unsigned int sX(sizeY*sizeZ);

	__shared__ P_REAL s_sh[BLOCK_SIZE*BLOCK_SIZE*BLOCK_SIZE];

	__shared__ P_REAL s_data[BLOCK_SIZE+2][BLOCK_SIZE+2][BLOCK_SIZE+2];

	// Index in array (shifted of 1 in each coordinate, as we want to avoid the boundary
	unsigned int index(((idX+1)*sizeY+idY+1)*sizeZ+idZ+1) ;
	unsigned int bindex((lidX*BLOCK_SIZE+lidY)*BLOCK_SIZE+lidZ);

	// Take care of array boundaries
	if(lidX==0){
		s_data[0][lidY+1][lidZ+1]=data[index-sX];
		s_data[BLOCK_SIZE+1][lidY+1][lidZ+1]=data[index+BLOCK_SIZE*sX];
	}
	if(lidY==0){
		s_data[lidX+1][0][lidZ+1]=data[index-sizeZ];
		s_data[lidX+1][BLOCK_SIZE+1][lidZ+1]=data[index+BLOCK_SIZE*sizeZ];
	}
	if(lidZ==0){
		s_data[lidX+1][lidY+1][0]=data[index-1];
		s_data[lidX+1][lidY+1][BLOCK_SIZE+1]=data[index+BLOCK_SIZE];
	}
	// Fill in interior
	s_data[lidX+1][lidY+1][lidZ+1]=data[index];

	__syncthreads();

	tmp[index] = aX*(s_data[lidX][lidY+1][lidZ+1]+s_data[lidX+2][lidY+1][lidZ+1])
		+aY*(s_data[lidX+1][lidY][lidZ+1]+s_data[lidX+1][lidY+2][lidZ+1])
		+aZ*(s_data[lidX+1][lidY+1][lidZ]+s_data[lidX+1][lidY+1][lidZ+2])
		-aF*f[index];

	P_REAL vtmp(cX*(s_data[lidX][lidY+1][lidZ+1]+s_data[lidX+2][lidY+1][lidZ+1])
		+cY*(s_data[lidX+1][lidY][lidZ+1]+s_data[lidX+1][lidY+2][lidZ+1])
		+cZ*(s_data[lidX+1][lidY+1][lidZ]+s_data[lidX+1][lidY+1][lidZ+2])
		-2.0f*(cX+cY+cZ)*s_data[lidX+1][lidY+1][lidZ+1]-f[index]);

	s_sh[bindex] = vtmp*vtmp;

	// Wait for all threads
	__syncthreads();

	// Take advantage of the geometry of the block
	// At each iteration, there are 7 points to consider
	// Use byte-shift to ease division by 2

	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(lidX < offset && lidY < offset && lidZ < offset){
			s_sh[bindex]+=s_sh[bindex+offset]+s_sh[bindex+offset*blockDim.z]+s_sh[bindex+offset*blockDim.z*blockDim.y]
					+s_sh[bindex+offset*(1+blockDim.z)]
					+s_sh[bindex+offset*(1+blockDim.z*blockDim.y)]
					+s_sh[bindex+offset*blockDim.z*(1+blockDim.y)]
					+s_sh[bindex+offset*(1+blockDim.z*(1+blockDim.y))];
		}

		// Wait for all threads
		__syncthreads();
	}

	// Write final result with (0,0,0) thread
	if(threadIdx.x==0 && threadIdx.y==0 && threadIdx.z == 0){
#if _GRID_DIM == DDD
		reduced[(blockIdx.x*gridDim.y+blockIdx.y)*gridDim.z+blockIdx.z] = s_sh[0];
#elif _GRID_DIM == DD
		reduced[blockIdx.x*gridDim.y*gridDim.x+blockIdx.y]=s_sh[0];
#endif
	}
}

__global__ void m_updateBoundaries(P_REAL* data, P_REAL* tmp, P_REAL* f, P_REAL* reduced, const unsigned int sizeX,const unsigned int sizeY,const unsigned int sizeZ, const P_REAL aX, const P_REAL aY, const P_REAL aZ, const P_REAL aF, const P_REAL cX, const P_REAL cY, const P_REAL cZ){

	// Global ID
#if _GRID_DIM == DDD
	int idX(blockDim.x*blockIdx.x+threadIdx.x);
	int idY(blockDim.y*blockIdx.y+threadIdx.y);
	int idZ(blockDim.z*blockIdx.z+threadIdx.z);
#elif _GRID_DIM == DD
	int tt(blockIdx.x%(gridDim.x*gridDim.y));
	int idX(blockDim.x*blockIdx.x+threadIdx.x);
	int idY(blockDim.y*(tt+threadIdx.y);
	int idZ(blockDim.z*((blockIdx.x-tt)/(gridDim.x*gridDim.y)+threadIdx.z);
#endif

	// First half of the entries assigned to "lower" domain, other half to "upper" domain
	if(threadIdx.x>=blockDim.x/2){
		idX+=sizeX-2-blockDim.x;
	}

	// Local ID
	int lidX(threadIdx.x);
	int lidY(threadIdx.y);
	int lidZ(threadIdx.z);


	const unsigned int sX(sizeY*sizeZ);

	__shared__ P_REAL s_sh[BLOCK_SIZE*BLOCK_SIZE*BLOCK_SIZE];

	__shared__ P_REAL s_data[BLOCK_SIZE+2][BLOCK_SIZE+2][BLOCK_SIZE+2];

	// Index in array (shifted of 1 in each coordinate, as we want to avoid the boundary
	unsigned int index(((idX+1)*sizeY+idY+1)*sizeZ+idZ+1) ;
	unsigned int bindex((lidX*BLOCK_SIZE+lidY)*BLOCK_SIZE+lidZ);

	// Take care of array boundaries
	if(lidX==0){
		s_data[0][lidY+1][lidZ+1]=data[index-sX];
		s_data[BLOCK_SIZE+1][lidY+1][lidZ+1]=data[index+BLOCK_SIZE*sX];
	}
	if(lidY==0){
		s_data[lidX+1][0][lidZ+1]=data[index-sizeZ];
		s_data[lidX+1][BLOCK_SIZE+1][lidZ+1]=data[index+BLOCK_SIZE*sizeZ];
	}
	if(lidZ==0){
		s_data[lidX+1][lidY+1][0]=data[index-1];
		s_data[lidX+1][lidY+1][BLOCK_SIZE+1]=data[index+BLOCK_SIZE];
	}
	// Fill in interior
	s_data[lidX+1][lidY+1][lidZ+1]=data[index];

	__syncthreads();

	tmp[index] = aX*(s_data[lidX][lidY+1][lidZ+1]+s_data[lidX+2][lidY+1][lidZ+1])
		+aY*(s_data[lidX+1][lidY][lidZ+1]+s_data[lidX+1][lidY+2][lidZ+1])
		+aZ*(s_data[lidX+1][lidY+1][lidZ]+s_data[lidX+1][lidY+1][lidZ+2])
		-aF*f[index];

	P_REAL vtmp(cX*(s_data[lidX][lidY+1][lidZ+1]+s_data[lidX+2][lidY+1][lidZ+1])
		+cY*(s_data[lidX+1][lidY][lidZ+1]+s_data[lidX+1][lidY+2][lidZ+1])
		+cZ*(s_data[lidX+1][lidY+1][lidZ]+s_data[lidX+1][lidY+1][lidZ+2])
		-2.0f*(cX+cY+cZ)*s_data[lidX+1][lidY+1][lidZ+1]-f[index]);

	s_sh[bindex] = vtmp*vtmp;

	// Wait for all threads
	__syncthreads();

	// Take advantage of the geometry of the block
	// At each iteration, there are 7 points to consider
	// Use byte-shift to ease division by 2

	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(lidX < offset && lidY < offset && lidZ < offset){
			s_sh[bindex]+=s_sh[bindex+offset]+s_sh[bindex+offset*blockDim.z]+s_sh[bindex+offset*blockDim.z*blockDim.y]
					+s_sh[bindex+offset*(1+blockDim.z)]
					+s_sh[bindex+offset*(1+blockDim.z*blockDim.y)]
					+s_sh[bindex+offset*blockDim.z*(1+blockDim.y)]
					+s_sh[bindex+offset*(1+blockDim.z*(1+blockDim.y))];
		}

		// Wait for all threads
		__syncthreads();
	}

	// Write final result with (0,0,0) thread
	if(threadIdx.x==0 && threadIdx.y==0 && threadIdx.z == 0){
		//TODO:Change this when general formula
#if _GRID_DIM == DDD
		reduced[blockIdx.y*gridDim.z+blockIdx.z] = s_sh[0];
#elif _GRID_DIM == DD
		reduced[blockIdx.y] = s_err[0];
#endif
	}
}

PoissonSolverJacobiCMPI::PoissonSolverJacobiCMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, int device,int rank,int neighborMin,int neighborMax):PoissonSolverJacobiC(sizeX,sizeY,sizeZ,device),ProblemMPI(rank,neighborMin,neighborMax){
}

void PoissonSolverJacobiCMPI::set(){
	PoissonProblem::set();
	P_REAL sum(m_delX*m_delX*m_delY*m_delY+m_delY*m_delY*m_delZ*m_delZ+m_delZ*m_delZ*m_delX*m_delX);
	sum *=2.0;
	m_j_ax = m_delY*m_delY*m_delZ*m_delZ/sum;
	m_j_ay = m_delX*m_delX*m_delZ*m_delZ/sum;
	m_j_az = m_delX*m_delX*m_delY*m_delY/sum;
	m_j_af = m_j_ax*m_delX*m_delX;

	// Convert to size_t
	size_t t((size_t)m_sizeX*(size_t)m_sizeY*(size_t)m_sizeZ);

	m_cX = 1.0f/m_delX;
	m_cY = 1.0f/m_delY;
	m_cZ = 1.0f/m_delZ;
	m_err = 0.0f;

	// Transfer data to GPU
	cudaMemcpy(d_data, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tmp, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_func, m_func, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
}

void PoissonSolverJacobiCMPI::iterate(){
	m_err = 0.0f;

	// Send border data through non-blocking signals
	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory sX(and size)
	int sX(m_sizeZ*m_sizeY);


#if _MPI_GPU_CONN == true
	if(m_neighborMin!=-1){
		MPI_Irecv(d_data,sX,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(d_data+sX,sX, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(d_data+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(d_data+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
#else
	if(m_neighborMin!=-1){
		cudaMemcpy(m_data+sX, d_data+sX, sX*sizeof(P_REAL), cudaMemcpyDeviceToHost);
		MPI_Irecv(m_data,sX,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+sX,sX, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		cudaMemcpy(m_data+(m_sizeX-2)*sX, d_data+(m_sizeX-2)*sX, sX*sizeof(P_REAL), cudaMemcpyDeviceToHost);
		MPI_Irecv(m_data+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
#endif
	// Take borders into account
	--m_gridSize.x;

	// Number of blocks
	size_t numBlocks(m_gridSize.x*m_gridSize.y*m_gridSize.z);

	// Compute on the GPU
	m_updateInterior<<<m_gridSize,m_blockSize>>>(d_data,d_tmp,d_func,d_reduced,m_sizeX,m_sizeY,m_sizeZ,m_j_ax,m_j_ay,m_j_az,m_j_af,m_cX,m_cY,m_cZ);

	// Pull back error
	cudaMemcpy(m_reduced, d_reduced, numBlocks*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	for(unsigned int i(0);i<numBlocks;i++){
		m_err+=m_reduced[i];
	}

	// Restore grid for next calls
	++m_gridSize.x;
	numBlocks+=m_gridSize.y*m_gridSize.z;

	// Wait for all sending procs
	MPI_Waitall(l,request,status);
#if _MPI_GPU_CONN != true
	if(m_neighborMin!=-1){
		cudaMemcpy(d_data,m_data,sX*sizeof(P_REAL),cudaMemcpyHostToDevice);
	}
	if(m_neighborMax!=-1){
		cudaMemcpy(d_data+(m_sizeX-1)*sX,m_data+(m_sizeX-1)*sX,sX*sizeof(P_REAL),cudaMemcpyHostToDevice);
	}
#endif
	dim3 gridSize(1,m_gridSize.y,m_gridSize.z);
	// Finish computation on border
	m_updateBoundaries<<<gridSize,m_blockSize>>>(d_data,d_tmp,d_func,d_reduced,m_sizeX,m_sizeY,m_sizeZ,m_j_ax,m_j_ay,m_j_az,m_j_af,m_cX,m_cY,m_cZ);


	// Pull back (p,Ap) (only first layer is actually interesting
	cudaMemcpy(m_reduced, d_reduced, m_gridSize.y*m_gridSize.z*sizeof(P_REAL), cudaMemcpyDeviceToHost);
	for(unsigned int i(0);i<m_gridSize.y*m_gridSize.z;i++){
		m_err+=m_reduced[i];
	}

	// Collect global error
	P_REAL gErr(0.0);
	MPI_Allreduce ( &m_err, &gErr, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );

	m_err=gErr;

	// swap addresses to apply changes
	P_REAL* swap(NULL);
	swap=d_data;
	d_data=d_tmp;
	d_tmp=swap;
}

