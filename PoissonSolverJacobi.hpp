#ifndef POISSONSOLVERJACOBI_HPP
#define POISSONSOLVERJACOBI_HPP
#include "PoissonProblem.hpp"
#include <cmath>
#define _X_MIN 0
#define _X_MAX 1
#define _Y_MIN 2
#define _Y_MAX 3
#define _Z_MIN 4
#define _Z_MAX 5

class PoissonSolverJacobi:public PoissonProblem{
	public:
		PoissonSolverJacobi(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ);

		void setBdry( P_REAL (*bdry)(P_REAL,P_REAL,P_REAL),unsigned int direction);
		void set();

		void iterate();

		P_REAL error() { return m_err;}


	protected:
		P_REAL* m_tmp;
		// Variables for jacobi iteration
		P_REAL m_j_ax;
		P_REAL m_j_ay;
		P_REAL m_j_az;
		P_REAL m_j_af;
		// Variables for error
		P_REAL m_err;
		//unsigned int m_nze;
};

#endif // POISSONSOLVERJACOBI_HPP
