#ifndef CUDADEF_HPP
#define CUDADEF_HPP

/**
 * Allow for providing default
 * NOTE:
 *  It would be dangerous to go higher, as 8 size in 1 direction results in 512 threads in one block, which is almost the limit of conventional GPUs.
 */
#ifndef BLOCK_SIZE
#define BLOCK_SIZE	8
#endif


// Allow MPI to write on GPU
// Doesn't work on local...
#ifndef _MPI_GPU_CONN
#define _MPI_GPU_CONN	false
#endif

// Dimension for grid in CUDA
// This is because Electra doesn't support 3D grids
#ifndef _GRID_DIM
#define _GRID_DIM	DD
#endif

#endif // CUDADEF_HPP
