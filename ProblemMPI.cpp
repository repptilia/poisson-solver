#include "ProblemMPI.hpp"

ProblemMPI::ProblemMPI(int rank, int neighborMin, int neighborMax):m_me(rank),m_neighborMin(neighborMin),m_neighborMax(neighborMax){}

void ProblemMPI::setNeighbor(int neighbor, unsigned int direction){
	if(direction==_X_MIN){
		m_neighborMin=neighbor;
	}
	if(direction==_X_MAX){
		m_neighborMax=neighbor;
	}
}
