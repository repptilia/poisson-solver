#ifndef POISSONSOLVERCGMPI_HPP
#define POISSONSOLVERCGMPI_HPP

#include "ProblemMPI.hpp"
#include "PoissonSolverCG.hpp"

class PoissonSolverCGMPI:public PoissonSolverCG, public ProblemMPI{
	public:
		PoissonSolverCGMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, P_REAL tol, int rank, int neighborMin, int NeighborMax);

		void set();
		void iterate();
};

#endif // POISSONSOLVERJACOBIMPI_HPP
