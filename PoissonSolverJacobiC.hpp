#ifndef POISSONSOLVERJACOBIC_HPP
#define POISSONSOLVERJACOBIC_HPP

#include "PoissonSolverJacobi.hpp"
#include "cudaDef.hpp"

class PoissonSolverJacobiC:public PoissonSolverJacobi{
	public:
		PoissonSolverJacobiC(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, int device);

		void set();
		void iterate();

		// Print data into stream
		// Needs overloading as the data stays in GPU memory throughout iterations
		virtual void printData(std::ostream& stream);

		~PoissonSolverJacobiC();
	protected:
		P_REAL m_cX;
		P_REAL m_cY;
		P_REAL m_cZ;
		// Structure variables
		dim3 m_blockSize;
		dim3 m_gridSize;
		// CUDA variables
		P_REAL* d_data;
		P_REAL* d_tmp;
		P_REAL* d_func;
		// For sum reductions
		P_REAL* d_reduced;
		// Local version
		P_REAL* m_reduced;
};

#endif // POISSONSOLVERCGC_HPP
