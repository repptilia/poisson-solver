#include <iostream>
#include <fstream>		// file
#include <sstream>		// num2str
#include <unistd.h>
#include <string.h>		// strcmp
#include <stdlib.h>		// atoi
#include <ctime>		// clock_t
#include <sys/time.h>		// gettimeofday
#include "PoissonSolverCGMPIT.hpp"
#include "PoissonSolverJacobiMPIT.hpp"

#define PROGRAM_NAME	"poisson_mpit"

// Possible iterators
enum Iterator { JACOBI, CG };

P_REAL g(P_REAL x, P_REAL y, P_REAL z){
	return 0.0;//return sin(M_PI*x)*sin(M_PI*y)*sin(M_PI*z);
}

P_REAL f(P_REAL x, P_REAL y, P_REAL z){
	return 1.0;//return sin(M_PI*x)*sin(M_PI*y)*sin(M_PI*z);
}

unsigned int showInvalid(){
	std::cout << "Invalid use of parameters. Check " << PROGRAM_NAME << " --help for more information." << std::endl;
	return 0;
}

int main(int argc,char** argv){
	// Total mesh size(N^3 values)
	int N(6);
	// Max number of iterations
	unsigned int iterations(10000);
	// Converging tolerance
	P_REAL tol(1e-3);
	// Verbose mode
	bool isVerbose(false);
	// Write to file
	bool writeOutput(false);
	std::string filename("");
	// Number of threads
	int numThreads(1);
	// Iterator
	Iterator it(JACOBI);
	int k(1);
	// Input parameter handling;
	while(k<argc){
		if(strcmp(argv[k],"-i")==0||strcmp(argv[k],"--iterations")==0){
			if(++k<argc){
				iterations=atoi(argv[k]);
			}
			else{
				return showInvalid();
			}
		}
		else if(strcmp(argv[k],"-m")==0||strcmp(argv[k],"--method")==0){
			if(++k<argc){
				if(strcmp(argv[k],"Jacobi")==0||strcmp(argv[k],"J")==0){
					it=JACOBI;
				}
				else if(strcmp(argv[k],"ConjugateGradient")==0||strcmp(argv[k],"CG")==0){
					it=CG;
				}
				else{
					std::cout << "Unknown iterator." << std::endl;
					return showInvalid();
				}
			}
			else{
				return showInvalid();
			}
		}
		else if(strcmp(argv[k],"-n")==0||strcmp(argv[k],"--threads")==0){
			if(++k<argc){
				numThreads=atoi(argv[k]);
			}
			else{
				return showInvalid();
			}
		}
		else if(strcmp(argv[k],"-o")==0||strcmp(argv[k],"--output")==0){
			if(++k<argc){
				filename=argv[k];
				writeOutput=true;
			}
			else{
				return showInvalid();
			}
		}
		else if(strcmp(argv[k],"-s")==0||strcmp(argv[k],"--size")==0){
			if(++k<argc){
				N=atoi(argv[k]);
			}
			else{
				return showInvalid();
			}
		}

		else if(strcmp(argv[k],"-t")==0||strcmp(argv[k],"--tol")==0){
			if(++k<argc){
				tol=atof(argv[k]);
			}
			else{
				return showInvalid();
			}
		}
		else if(strcmp(argv[k],"-v")==0||strcmp(argv[k],"--verbose")==0){
			isVerbose=true;
		}
		else if(strcmp(argv[k],"-h")==0||strcmp(argv[k],"--help")==0){
			std::cout << "\
Usage: " << PROGRAM_NAME << "[options]\n\
Options :\n\
	-h,--help				show this menu\n\
	-i,--iterations	<numberOfIterations>	max number of iterations 	(positive integer)\n\
	-m, --method <methodName>		iterative method used		(chain of characters)\n\
	-n,--threads <numberOfThreads>		number of threads per program	(positive integer)\n\
	-o,--output <filename>			output file to write data to 	(chain of characters)\n\
	-s,--size <meshSize>			total mesh width 		(positive integer)\n\
	-t,--tol <tolerance>			tolerance of convergence 	(P_REAL-precision number)\n\
	-v,--verbose				toogle verbose mode\n\
\n\
" << PROGRAM_NAME << " by Etienne Favre etienne.favre@epfl.ch\n\
Please feel free to mail me about bug reports"
			<< std::endl;
			return 0;
		}
		else{
			return showInvalid();
		}
		++k;
	}
	// MPI part
	int rank,size,error(0);
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Comm_size( MPI_COMM_WORLD, &size );

	// Defaults to 5 times the number of processes
	if(N==0){
		N=5*size;
	}
	// Compute the size of the precision
	P_REAL delX(1.0/(P_REAL)(N-1));
	// Compute the size of the x-process s.t the result is the same as in 1-process case
	unsigned int l(0);
	while((l+1)*delX<1.0/(P_REAL)size){
		l++;
	}
	unsigned int xSize(l+2);
	P_REAL xMin(rank*l*delX),xMax(((rank+1)*l+1)*delX);
	if(rank==size-1 && xMax != 1){
		xMax=1;
		xSize=N-(size-1)*l;
	}
	// Prevent execution of too small program
	if(xSize < 4){
		error=1;
	}
	// Broadcast error (if there is one)
	// NOTE: we know that the smallest layer is the first, so setting the broadcast from the first.
	// Using a random process could lead to a deadlock
	MPI_Bcast(&error, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if(error != 0){
		if(rank == 0){
			std::cout << "Some layers have size " << xSize << " with the current number of MPI processes.\nTry with less processes, or increase the mesh refinement." << std::endl;
		}
		MPI_Finalize();
		return 0;
	}
	// Flexible solver
	PoissonProblem* p;
	// Configure its neighbors
	int nMin(-1);
	int nMax(-1);
	if(rank>0){
		nMin = rank-1;
	}
	if(rank<size-1){
		nMax = rank+1;
	}
	switch(it){
		case CG:
			p=new PoissonSolverCGMPIT(xSize,N,N,tol,rank,nMin,nMax,numThreads);
			break;

		case JACOBI:
			p=new PoissonSolverJacobiMPIT(xSize,N,N,rank,nMin,nMax,numThreads);
			break;

		default:
			break;
	}
	p->xLim(xMin,xMax);
	p->yLim(0,1);
	p->zLim(0,1);
	if(rank==0){
		p->setBdry(&f,0);
	}
	if(rank==size-1){
		p->setBdry(&f,1);
	}
	p->setBdry(&f,2);
	p->setBdry(&f,3);
	p->setBdry(&f,4);
	p->setBdry(&f,5);
	p->setFunc(&g);

	p->set();

	// Iteration variable
	unsigned int i(0);
	// Stopper
	bool stop(false);
	P_REAL change(0.0);
	// Start timer
	std::clock_t start(std::clock());
	struct timeval timeStart;
	gettimeofday(&timeStart,NULL);
	while(i<iterations&&!stop){
		p->iterate();
		change=p->error();
		if(change<tol){
			stop=true;
		}

		if(isVerbose && i%10==0 && rank==0){
			std::cout << "Iteration " << i+1 << ": " << change << std::endl;
		}
		i++;
	}
	if(writeOutput){
		std::ostringstream c;
		c << rank;

		filename+="_"+c.str();

		std::ofstream file(filename.c_str());
		file << *p;

		file.close();
	}
	if(rank==0){
		struct timeval timeEnd;
		gettimeofday(&timeEnd,NULL);
		std::cout << "Solver took -CPU time: " << (std::clock()-start)/(P_REAL)CLOCKS_PER_SEC << " s\n"
			  << "            -Wall time:" << (timeEnd.tv_sec-timeStart.tv_sec) + (timeEnd.tv_usec - timeStart.tv_usec)/1000000.0 << " s" << std::endl;
	}

	MPI_Finalize();
	return 0;
}
