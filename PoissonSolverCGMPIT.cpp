#include "PoissonSolverCGMPIT.hpp"

PoissonSolverCGMPIT::PoissonSolverCGMPIT(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, P_REAL tol, int rank, int neighborMin, int neighborMax, size_t numThreads): PoissonSolverCGMPI(sizeX,sizeY,sizeZ,tol,rank,neighborMin,neighborMax), m_numThreads(numThreads){
	pthread_mutex_init( &m_mutex, NULL);
	pthread_cond_init( &m_cond, NULL);
}

void PoissonSolverCGMPIT::set(){
	m_cX = 1.0/m_delX;
	m_cY = 1.0/m_delY;
	m_cZ = 1.0/m_delZ;
	m_normr = 0.0;

	m_del=0;
	while(m_del*m_numThreads<m_sizeZ-2){
		m_del++;
	}

	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory sX(and size)
	int sX(m_sizeZ*m_sizeY);
	if(m_neighborMin!=-1){
		MPI_Irecv(m_data,sX,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+sX,sX, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_data+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
	unsigned int b(2*sX+m_sizeZ);
	for(unsigned int i(2);i<m_sizeX-2;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_r[b+k]=m_func[b+k]-m_cX*(m_data[b+k-sX]+m_data[b+k+sX]-2.0*m_data[b+k])
						    -m_cY*(m_data[b+k-m_sizeZ]+m_data[b+k+m_sizeZ]-2.0*m_data[b+k])
						    -m_cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0*m_data[b+k]);
				m_p[b+k]=m_r[b+k];
				m_normr+=m_r[b+k]*m_r[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Wait for all the tasks
	MPI_Waitall(l,request,status);
	// Now that we have data, update boundaries
	b=m_sizeZ;
	unsigned int z((m_sizeX-2)*sX);
	for(unsigned int j(1);j<m_sizeY-1;j++){		
		for(unsigned int k(1);k<m_sizeZ-1;k++){
			m_r[sX+b+k]=m_func[sX+b+k]-m_cX*(m_data[b+k]+m_data[b+k+2*sX]-2.0*m_data[sX+b+k])
					    -m_cY*(m_data[sX+b+k-m_sizeZ]+m_data[sX+b+k+m_sizeZ]-2.0*m_data[sX+b+k])
					    -m_cZ*(m_data[sX+b+k-1]+m_data[sX+b+k+1]-2.0*m_data[sX+b+k]);
			m_p[sX+b+k]=m_r[sX+b+k];
			m_normr+=m_r[sX+b+k]*m_r[sX+b+k];
			m_r[z+b+k]=m_func[z+b+k]-m_cX*(m_data[z+b+k-sX]+m_data[z+b+k+sX]-2.0*m_data[z+b+k])
					    -m_cY*(m_data[z+b+k-m_sizeZ]+m_data[z+b+k+m_sizeZ]-2.0*m_data[z+b+k])
					    -m_cZ*(m_data[z+b+k-1]+m_data[z+b+k+1]-2.0*m_data[z+b+k]);
			m_p[z+b+k]=m_r[z+b+k];
			m_normr+=m_r[z+b+k]*m_r[z+b+k];
		}
		b+=m_sizeZ;
	}
	// Collect global (p,Ap)
	P_REAL gnormr(0.0);
	MPI_Allreduce ( &m_normr, &gnormr, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
	m_normr=gnormr;
}

void PoissonSolverCGMPIT::iterate(){
	// Initialize globalized
	m_pAp   = 0.0;
	m_alpha = 0.0;
	m_beta  = 0.0;
	m_rNew  = 0.0;
	// Block threads
	m_blockThreads = true;
	// Meanwhile, launch pthreads to compute interior of domain
	// Create threads
	pthread_t* threads = (pthread_t*)malloc(m_numThreads*sizeof(pthread_t));
	t_cg_data* data = (t_cg_data*)malloc(m_numThreads*sizeof(t_cg_data));
	m_finishedThreads = 0;
	for(unsigned int t(0);t<m_numThreads;t++){
		data[t].pb = this;
		data[t].rank=t;
		int rc(pthread_create(&threads[t],NULL,&PoissonSolverCGMPIT::t_iterate,(void*)&data[t]));
		if(rc){
			std::cout << "Error from pthread_create : " << rc << std::endl;
		}
	}
	// Compute Ap and (p,Ap) in the same time
	// Send border data through non-blocking signals
	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory sX(and size)
	int sX(m_sizeZ*m_sizeY);
	if(m_neighborMin!=-1){
		MPI_Irecv(m_p,sX,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_p+sX,sX, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_p+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_p+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
	// Wait for all the tasks
	MPI_Waitall(l,request,status);
	// Broadcast signal ready
	m_blockThreads = false;
	pthread_cond_broadcast(&m_cond);

	for(unsigned int t(0);t<m_numThreads;t++){
		pthread_join(threads[t],NULL);
	}
}

void PoissonSolverCGMPIT::iteratePartial(size_t rank){
	// Divise units where integration is needed
	unsigned int sX(m_sizeY*m_sizeZ);
	// Boundaries for z
	unsigned int min(m_del*rank+1);
	unsigned int max=(1+m_del*(rank+1))<m_sizeZ-1?1+m_del*(rank+1):m_sizeZ-1;
	// To keep locking mutex only once, use partial values;
	P_REAL pAp(0.0);
	unsigned int b(2*sX+m_sizeZ);
	for(unsigned int i(2);i<m_sizeX-2;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(min);k<max;k++){
				m_Ap[b+k]=m_cX*(m_p[b+k-sX]+m_p[b+k+sX]-2.0*m_p[b+k])
					 +m_cY*(m_p[b+k-m_sizeZ]+m_p[b+k+m_sizeZ]-2.0*m_p[b+k])
					 +m_cZ*(m_p[b+k-1]+m_p[b+k+1]-2.0*m_p[b+k]);
				pAp+=m_p[b+k]*m_Ap[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Wait for boundary data
	pthread_mutex_lock(&m_mutex);
	if(m_blockThreads){
		pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);

	// Now, update boundary
	b=m_sizeZ;
	unsigned int z((m_sizeX-2)*sX);
	for(unsigned int j(1);j<m_sizeY-1;j++){		
		for(unsigned int k(min);k<max;k++){
			m_Ap[sX+b+k]=m_cX*(m_p[b+k]+m_p[b+k+2*sX]-2.0*m_p[sX+b+k])
				    +m_cY*(m_p[sX+b+k-m_sizeZ]+m_p[sX+b+k+m_sizeZ]-2.0*m_p[sX+b+k])
				    +m_cZ*(m_p[sX+b+k-1]+m_p[sX+b+k+1]-2.0*m_p[sX+b+k]);
			pAp+=m_p[sX+b+k]*m_Ap[sX+b+k];
			m_Ap[z+b+k]=m_cX*(m_p[z-sX+b+k]+m_p[z+b+k+sX]-2.0*m_p[z+b+k])
				   +m_cY*(m_p[z+b+k-m_sizeZ]+m_p[z+b+k+m_sizeZ]-2.0*m_p[z+b+k])
				   +m_cZ*(m_p[z+b+k-1]+m_p[z+b+k+1]-2.0*m_p[z+b+k]);
			pAp+=m_p[z+b+k]*m_Ap[z+b+k];
		}
		b+=m_sizeZ;
	}

	// Wait for all threads
	pthread_mutex_lock(&m_mutex);
	m_pAp+=pAp;
	m_finishedThreads++;
	// On finishing thread, prepare next batch
	if(m_finishedThreads==m_numThreads){
		// Reduce (p,Ap)
		P_REAL gpAp(0);
		MPI_Allreduce ( &m_pAp, &gpAp, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
		// Compute the coefficient alpha
		m_alpha=m_normr/gpAp;
		// Release all threads
		m_finishedThreads = 0;
		pthread_cond_broadcast(&m_cond);
	}
	else{
		pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);

	// Update x,r and compute the new residual norm
	P_REAL rNew(0.0);
	b=sX+m_sizeZ;
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(min);k<max;k++){
				m_data[b+k]+=m_alpha*m_p[b+k];
				m_r[b+k]-=m_alpha*m_Ap[b+k];
				rNew+=m_r[b+k]*m_r[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}

	// Wait for all threads
	pthread_mutex_lock(&m_mutex);
	m_rNew+=rNew;
	m_finishedThreads++;
	// On finishing thread, prepare next batch
	if(m_finishedThreads==m_numThreads){
		// Reduce (p,Ap)
		P_REAL grNew(0);
		MPI_Allreduce ( &m_rNew, &grNew, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
		m_rNew = grNew;
		//TODO
		// If convergence reached, stop
		//if(grNew<m_tol){
		//	m_normr=grNew;
		//	return;
		//}

		// Update p, residual norm if convergence not reached
		m_beta = m_rNew/m_normr;
		m_normr = m_rNew;
		// Release all threads
		m_finishedThreads = 0;
		pthread_cond_broadcast(&m_cond);
	}
	else{
		pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);

	b=sX+m_sizeZ;
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(min);k<max;k++){
				m_p[b+k]=m_r[b+k]+m_beta*m_p[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
}

void* PoissonSolverCGMPIT::t_iterate(void* item){
	t_cg_data* data = new t_cg_data();
	data = (t_cg_data *)item;
	data->pb->iteratePartial(data->rank);

	pthread_exit(NULL);
}

PoissonSolverCGMPIT::~PoissonSolverCGMPIT(){
	pthread_mutex_destroy(&m_mutex);
}
