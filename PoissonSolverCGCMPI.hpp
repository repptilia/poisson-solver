#ifndef POISSONSOLVERCGCMPI_HPP
#define POISSONSOLVERCGCMPI_HPP

#include "PoissonSolverCGC.hpp"
#include "ProblemMPI.hpp"

/**
 * class PoissonSolverCGCMPI
 * -------------------------
 * CUDA-based solver using conjugate gradient in MPI process
 *
 * NOTE:
 * -The grid size changes from the mother class. 
 *  To take borders into account, its size in X direction is reduced by 1
 *  Therefore, the first updating only "misses" one layer of blocks
 *  And when we get boundaries, we use remaining layer split in two
 */

class PoissonSolverCGCMPI:public PoissonSolverCGC, public ProblemMPI{
	public:
		PoissonSolverCGCMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,P_REAL tol, int device,int rank,int neighborMin,int neighborMax);

		void set();
		void iterate();
	protected:
};

#endif // POISSONSOLVERCGCMPI_HPP
