#include "PoissonProblem.hpp"

PoissonProblem::PoissonProblem(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ) : m_sizeX(sizeX), m_sizeY(sizeY), m_sizeZ(sizeZ){
	// Convert to size_t
	size_t t((size_t)sizeX*(size_t)sizeY*(size_t)sizeZ);
	m_data = (P_REAL*)calloc(t,sizeof(P_REAL));
	m_func = (P_REAL*)calloc(t,sizeof(P_REAL));

	m_maxIt = (size_t)sizeZ*((size_t)sizeY*((size_t)sizeX-2)-2)-2;
}

void PoissonProblem::print(){
	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				std::cout << m_data[b+k] << " ";
			}
			std::cout << std::endl;
			b+=m_sizeZ;
		}
		std::cout << std::endl;
	}
	std::cout <<"dim:" << m_sizeX << m_sizeY << m_sizeZ << std::endl;
}

void PoissonProblem::printF(){
	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				std::cout << m_func[b+k] << " ";
			}
			std::cout << std::endl;
			b+=m_sizeZ;
		}
		std::cout << std::endl;
	}
	std::cout <<"dim:" << m_sizeX << m_sizeY << m_sizeZ << std::endl;
}

void PoissonProblem::printD(){
	std::cout << "Range" << std::endl << "-x: " << m_corner[_X_MIN] << " -> " << m_corner[_X_MAX] << ", dX: " << m_delX << " (" << m_sizeX << " steps)"
	<< std::endl << "-y: " << m_corner[_Y_MIN] << " -> " << m_corner[_Z_MAX] << ", dY: " << m_delY << " (" << m_sizeY << " steps)"
	<< std::endl << "-z: " << m_corner[_Z_MIN] << " -> " << m_corner[_Z_MAX] << ", dZ: " << m_delZ  << " (" << m_sizeZ << " steps)" << std::endl;
}

void PoissonProblem::xLim(P_REAL min,P_REAL max){
	m_corner[_X_MIN]=min;
	m_corner[_X_MAX]=max;
	m_delX = (m_corner[_X_MAX]-m_corner[_X_MIN])/(m_sizeX-1.0);
	m_cX = 1.0f/m_delX;
}

void PoissonProblem::yLim(P_REAL min,P_REAL max){
	m_corner[_Y_MIN]=min;
	m_corner[_Y_MAX]=max;
	m_delY = (m_corner[_Y_MAX]-m_corner[_Y_MIN])/(m_sizeY-1.0);
	m_cY = 1.0f/m_delY;
}

void PoissonProblem::zLim(P_REAL min,P_REAL max){
	m_corner[_Z_MIN]=min;
	m_corner[_Z_MAX]=max;
	m_delZ = (m_corner[_Z_MAX]-m_corner[_Z_MIN])/(m_sizeZ-1.0);
	m_cZ = 1.0f/m_delZ;
}

void PoissonProblem::set(){
}

void PoissonProblem::setFunc( P_REAL (*func)(P_REAL,P_REAL,P_REAL)){
	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				m_func[b+k]=func(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]+k*m_delZ);
			}
			b+=m_sizeZ;
		}
	}
}

void PoissonProblem::setBdry( P_REAL (*bdry)(P_REAL,P_REAL,P_REAL),unsigned int direction){
	switch(direction){
		case _X_MIN:
			for(unsigned int j(0);j<m_sizeY;j++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[j*m_sizeZ+k]=bdry(m_corner[_X_MIN],m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]+k*m_delZ);
				}
			}
			break;
		case _X_MAX:
			for(unsigned int j(0);j<m_sizeY;j++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[((m_sizeX-1)*m_sizeY+j)*m_sizeZ+k]=bdry(m_corner[_X_MAX],m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]+k*m_delZ);
				}
			}
			break;
		case _Y_MIN:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[i*m_sizeY*m_sizeZ+k]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN],m_corner[_Z_MIN]+k*m_delZ);
				}
			}
			break;
		case _Y_MAX:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int k(0);k<m_sizeZ;k++){
					m_data[i*m_sizeY*m_sizeZ+m_sizeZ*(m_sizeY-1)+k]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MAX],m_corner[_Z_MIN]+k*m_delZ);
				}
			}
			break;
		case _Z_MIN:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int j(0);j<m_sizeY;j++){
					m_data[(i*m_sizeY+j)*m_sizeZ]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MIN]);
				}
			}
			break;
		case _Z_MAX:
			for(unsigned int i(0);i<m_sizeX;i++){
				for(unsigned int j(0);j<m_sizeY;j++){
					m_data[(i*m_sizeY+j+1)*m_sizeZ-1]=bdry(m_corner[_X_MIN]+i*m_delX,m_corner[_Y_MIN]+j*m_delY,m_corner[_Z_MAX]);
				}
			}
			break;
	}
}

void PoissonProblem::printData(std::ostream& stream){
	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				stream << m_data[b+k] << " ";
			}
			stream << std::endl;
			b+=m_sizeZ;
		}
		stream << std::endl;
	}
}

std::ostream& operator<<(std::ostream& out, PoissonProblem& p){
	p.printData(out);
	return out;
}

