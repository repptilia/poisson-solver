#ifndef POISSONSOLVERJACOBIMPIT_HPP
#define POISSONSOLVERJACOBIMPIT_HPP

#include <pthread.h>
#include "PoissonSolverJacobiMPI.hpp"

// Poisson Problem using pthreads

class PoissonSolverJacobiMPIT:public PoissonSolverJacobiMPI{
	public:
		PoissonSolverJacobiMPIT(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,int rank,int neighborMin,int neighborMax,size_t numThreads);

		void iterate();
		// Called in thread
		void iteratePartial(size_t rank);

		// Static method to be called in thread
		static void* t_iterate(void* item);

	protected:
		size_t m_numThreads;
		size_t m_finishedThreads;
		// Bool variable blocking if MPI requires to wait
		bool m_blockThreads;
		// condition variable
		pthread_cond_t m_cond;
		// mutex
		pthread_mutex_t m_mutex;
};

struct t_data{
	PoissonSolverJacobiMPIT* pb;
	size_t rank;
};

#endif // POISSONSOLVERJACOBIMPIT_HPP
