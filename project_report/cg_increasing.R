require(tikzDevice)
require(reshape)
require(ggplot2)
require(ggthemes)

cdata=read.table("cg_increasing.txt") 
colnames(cdata)=c("Threads","MPI","pthreads", "Predicted")
pdata=melt(cdata,id.vars="Threads",variable_name = "Type")

do_plot<-ggplot(pdata,aes(x=Threads,y=value,color=Type, shape=Type)) + 
  geom_point() + geom_line() +
  scale_x_log10(breaks=2^(0:4)) + 
  ylab('Speed-up') +
  scale_color_colorblind()+ opts(
  legend.position = c(0.2,0.7),
  legend.background = theme_rect(fill = "#ffffffaa", colour = NA))

tikz("cg_increasing.tex", width=3, height=2.5)
do_plot
dev.off()
