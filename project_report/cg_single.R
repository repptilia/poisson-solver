require(tikzDevice)
require(reshape)
require(ggplot2)
require(ggthemes)

cdata=read.table("cg_single.txt") 
colnames(cdata)=c("Threads","MPI","pthreads","Predicted")
pdata=melt(cdata,id.vars="Threads",variable_name = "Type")

do_plot<-ggplot(pdata,aes(x=Threads,y=value,color=Type, shape=Type)) + 
  geom_point() + geom_line() +
  scale_x_log10(breaks=2^(0:4)) + 
  ylab('Time (s)') +
  scale_color_colorblind()+ opts(
  legend.position = c(0.1,0.2),
  legend.background = theme_rect(fill = "#ffffffaa", colour = NA)
)

tikz("cg_single.tex", width=4, height=2.5)
do_plot
dev.off()
