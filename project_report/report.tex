\documentclass[a4wide,12pt]{article}
\usepackage{a4wide}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel} 
\usepackage{url}

\usepackage{fancyvrb}
\usepackage{amsmath}
\usepackage{amssymb}	


\usepackage{tikz}


\date{}


\title{Poisson Equation Solvers}

\author{Etienne Favre}

\definecolor{dkgreen}{rgb}{.13,.57,.33}
\definecolor{dkred}{rgb}{.57,.13,.33}


\begin{document}

\maketitle

\begin{abstract}
In numerical analysis, an important class of problems is the  solving of PDE equations. One of those equations is the Poisson equation. In this paper are presented methods for solving Poisson equation, and parallelized versions, with performance optimizations. Resulting programs are then compared.
\end{abstract}

\section{Problem definition}

Consider the domain $B=]0,1[^3\subset\mathbb{R}^3$, $f,g$ two sufficiently regular functions. The Poisson Problem
\[
	\begin{cases}
		\Delta u=f &f\;\mathrm{on}\;B\\
		u=g &g\,\mathrm{on}\;\partial B
	\end{cases}
\]
Using finite-differences approximation, one can write
\[
	\frac{\partial u}{\partial x_i}(x)\simeq\frac{u(x-he_i)-2u(x)+u(x+he_i)}{h^2}
\]
Therefore, given $N\in\mathbb{N}$ and $h=\frac{1}{N-1}$, and the notation
\[
	v_{i\,j\,k}=u(hie_1,hje_2,hje_3)
\]
One can write the problem as 
\[
	\frac{1}{h^2}\left(v_{i-1\,j\,k}+v_{i+1\,j\,k}+v_{i\,j-1\,k}+v_{i\,j+1\,k}
	+v_{i\,j\,k-1}+v_{i\,j\,k+1}-8v_{i\,j\,k}\right)=f_{i\,j\,k}
\]
Therefore, one can express the initial problem of the form
\[
	Au=b
\]
letting $v\in\mathbb{R}^{N^3}$ defined as 
\[
	u_{iN^2+jN+k}=v_{i\,j\,k}
\]
Where $A$ is a sparse matrix of size $N^6$.

\noindent This leads to the presentation of some solving methods.

\subsection{Jacobi method}
Let $A=D+R$, where $D$ is the diagonal of $A$. Then, as $(D+R)u=b$, one gets
\[
	u=D^{-1}(b-Ru)
\]
Which leads to an iterative formulation :
\begin{itemize}
	\item Set $u^{(0)}$
	\item Compute
	\[
		v_{i\,j\,k}^{(n+1)}=\frac{1}{8}\left(h^2f_{i\,j\,k}-v_{i-1\,j\,k}^{(n)}-v_{i+1\,j\,k}^{(n)}-v_{i\,j-1\,k}^{(n)}-v_{i\,j+1\,k}^{(n)}-v_{i\,j\,k-1}^{(n)}-v_{i\,j\,k+1}^{(n)}\right)
	\]
\end{itemize}
\subsubsection{Convergence}
As
\begin{align*}
	u^{(n+1)}-u^{(n)}&=D^{-1}(b-Ru^{(n)})-D^{-1}(b-Ru^{(n-1)})\\
	&=-D^{-1}Ru^{(n)}+D^{-1}Ru^{(n-1)}=-D^{-1}R(u^{(n)}-u^{(n-1)})
\end{align*}
Let $x\in\mathbb{R}^{N^3}$.
\begin{align*}
	(D^{-1}Rx)^2_i&=\frac{1}{h^2}\left(\frac{1}{8}\left(x_{i-N^2}+x_{i+N^2}+x_{i+N}+x_{i-N}+x_{i+1}+x_{i-1}\right)\right)^2\\
	&=\frac{1}{64h^2}\left(x^2_{i+N^2}+\ldots+2x_{i-N^2}x_{i+N^2}+\ldots\right)\\
\end{align*}
(Where $x_r=0$, where $r$ is out of range)
Then
\begin{align*}
	\|Rx\|&=\frac{1}{h^2}\sum_{i=0}^{N^3-1}(Rx_i)^2\\
	&=\frac{1}{64h^2}\left(x^2_{i+N^2}+\ldots+2x_{i-N^2}x_{i+N^2}+\ldots\right)\\
	&\leq\frac{1}{64h}\left(5\|x\|+10\|x\|\right)\\
	&\leq\frac{1}{2h}\|x\|
\end{align*}
Thus $\|D^{-1}R\|<15/64$ so we expect a quite slow convergence, since mesh-dependent.

\subsection{Conjugate Gradient method}
Let $\{p_i\}$ be a set of conjugate vectors of $\mathbb{R}^{N^3}$ with respect to $A$ (that is, $p_j^*Ap_i=0$ for $i\neq j$), and $u^*$ be a solution of $Au=b$. Then
\[
	b_i=p_i^*b=p_i^*(Au)=p_i^*\left(\sum\alpha_jA\,p_j\right)=\alpha_ip_i^*Ap_i
\]
So $\alpha_i=\frac{p_i^*b_i}{p_i^*Ap_i}$. The idea of the conjugate gradient method is to gradually construct those vectors $p_i$, and approximate $u$ as a linear combination of those, exploiting the fact that the $\{p_i\}$ are conjugate.\\

\noindent The resulting algorithm is:\\

Let $r^{(0)}=b-Au^{(0)},p^{(0)}=r_0,k=0$
\begin{itemize}
	\item $\alpha_k=\frac{r^{(k)\,*r^{(k)}}}{p^{(k)\,*}Ap^{(k)}}$
	\item $u^{(k+1)}=u^{(k)}+\alpha_kp^{(k)}$
	\item $r^{(k+1)}=r^{(k)}-\alpha_kAp^{(k)}$
	\item If $r^{(k+1)}<\mathrm{tol}$, stop, else
	\item $\beta_k=\frac{r^{(k+1)\,*}r^{(k+1)}}{r^{(k)\,*}r^{(k)}}$
	\item $p^{(k+1)}=r^{(k+1)}+\beta_kp^{(k)}$
	\item $k+=1$
\end{itemize}

\subsubsection{Convergence}
Let $u_*$ be the exact solution. It can be shown \cite[pp.~214-215]{convExp} that
\[
	\|u^{(m)}-u_*\|\leq2\left(\frac{\sqrt{\kappa}-1}{\sqrt{\kappa}+1}\right)^m\|u^{(0)}-u_*\|
\]
Where $\kappa=\kappa(A)=O(h^{-2})$. Therefore, slow convergence is as well expected, as decreasing in function on the mesh size.

\section{Parallelization}
The two method presented above are iterative, and require to advance to the next step mostly matrix-vector multiplications. These operations are therefore very well suited for parallelization. First, the Jacobi method was parallelized, but then, given the similarity (in the algorithm) of the Conjugate Gradient method, it has been decided to add as well this method to the program.\\

In order to be flexible, a POO approach has been used and classes have been created for each solver. Therefore, there is a possibility of a hybrid coupling between different solvers. Altough the project is mainly focused on MPI and pthreads, a CUDA solver is as well presented.

\noindent To achieve parallelism, multiple paradigms have been used :

\subsection{MPI}
In the program, MPI is used to slice the cubic domain in rectangles along the $x$ axis. That is, we only slice in one direction.

\noindent While the methods are easy to parallelize using a shared memory model, using MPI and a distributed memory model requires little attention. Indeed, in both methods (for $v$ in Jacobi and $p$ in CG), one needs, to iterate the vector, to know the values of the neighboring elements. Therefore, each process needs to have access to the first layer of data above and under him, as shown in Fig.~\ref{fMPIcomm}. The reason why the domain is sliced only in one direction is to allow all communication being handled in only 2 transactions (See Fig.~\ref{MPIicomm})\\

\begin{figure}[!htbl]
\begin{Verbatim}
	if(m_neighborMin!=-1){
		MPI_Irecv(m_p,sX,_MPI_DATATYPE,m_neighborMin,
		0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_p+sX,sX, _MPI_DATATYPE,m_neighborMin,
		1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_p+(m_sizeX-1)*sX,sX,_MPI_DATATYPE,m_neighborMax,
		1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_p+(m_sizeX-2)*sX,sX ,_MPI_DATATYPE,m_neighborMax,
		0,MPI_COMM_WORLD,request+l++ );
	}
\end{Verbatim}
\caption{Data sending/receiving throuh non-blocking communications.}\label{MPIicomm}
\end{figure}


\begin{figure}\label{fMPIcomm}
\centering
\begin{tikzpicture}
\draw (10.5,0) -- (0,0) -- (0,3) -- (10.5,3) -- cycle;
\draw[blue] (10,2.5) -- (10,0.5) -- (5.5,0.5) -- (5.5,2.5) -- cycle;
\draw[blue] (0.5,2.5) -- (0.5,0.5) -- (5,0.5) -- (5,2.5) -- cycle;
\draw[step=0.5,black,opacity=0.1] (0,0) grid(10.5,3);
\draw[blue] (2.5,1.5) node[align=center]{Interior 1};
\draw[blue] (8,1.5) node[align=center]{Interior 2};

\draw[red,opacity=0.5] (4.8,-0.2) -- (5.7,-0.2) -- (5.7,3.2) -- (4.8,3.2) -- cycle;
\draw[red] (5.25,3.4) node{Shared vertices};
\draw[green,opacity=0.8,dashed, thick] (0,0) -- (5.5,0) -- (5.5,3) -- (0,3) -- cycle;
\draw[purple,dashed, thick] (10.5,0) -- (5,0) -- (5,3) -- (10.5,3) -- cycle;
\end{tikzpicture}
\caption{$2$D grid schematics of MPI memory sharing. The vertices represent the computation points}
\end{figure}

\noindent While the data is being exchanged, the interior of the domain is being computed, and then the border data are computed.

\noindent The second place where communication is needed is in the conjugate gradient method, where $\alpha_k$ and $\beta_k$ must be globally accessible. To achieve this, first note that by construction, they are easy to implement locally. The process of summing and distributing is achieved  as shown in Fig.~\ref{MPIicomm2}

\begin{figure}[!htbl]
\begin{Verbatim}
	double gpAp(0.0);
	MPI_Allreduce ( &pAp, &gpAp, 1, _MPI_DATATYPE,
	 MPI_SUM,MPI_COMM_WORLD );
	double alpha(m_normr/gpAp);
\end{Verbatim}
\caption{Computing a global coefficient. Here, in conjugate gradient algorithm, the reduction of $p\cdot Ap$, to compute $\alpha^{(k)}$.}\label{MPIicomm2}
\end{figure}

\noindent With the use of those two techniques, a program has been created (poisson\_mpi).

\subsection{pthreads}
Inside a MPI process, pthreads are launched to parallelize computation. While the memory is this time shared, another difficulty is introduced, that is, thread synchronization.\\

\noindent Using pthreads to slice the domain in another direction, the data exchange between processes needs to be taken into account. A thread needs to wait before data is sent from another process, and then acknowledge and compute its next part. This is achieved by the use of condition variables (See Fig.~\ref{pthreadCondVar}).

\begin{figure}[!htbl]
\begin{Verbatim}
	// Wait for boundary data
	pthread_mutex_lock(&m_mutex);
	if(m_blockThreads){
		pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);
\end{Verbatim}
\caption{Use of condition variables to block threads. Here, m\_blockThreads is set to true until the data from boundaries has been exchanged with MPI.}\label{pthreadCondVar}
\end{figure}

Once the synchronization has been taken care of, the application is quite straightforward. A slight trick though can be presented. As the program was created with oriented-object paradigm, came the problem of creating thread functions. As this cannot be done directly using a class member, which would be very handy to keep stored iteration variables, a static function has been created (see Fig.~\ref{pthreadTrick}) to act as intermediary.

\begin{figure}[!htbl]
\begin{Verbatim}
void* PoissonSolverCGMPIT::t_iterate(void* item){
	t_cg_data* data = new t_cg_data();
	data = (t_cg_data *)item;
	data->pb->iteratePartial(data->rank);
	pthread_exit(NULL);
}
\end{Verbatim}
\caption{Static thread function called in the pthreads.}\label{pthreadTrick}
\end{figure}


\subsection{CUDA}
Most computers are multi-core and handle GPUs. Therefore, it might be interesting to run some MPI instances of the problem on the GPU. To such end, a simple solver was first designed.

\noindent The concept is relatively straightforward : create a 3D grid of thread blocks, of dimension $8\times8\times8$ and let each thread handle a computation. The very basic idea is quite slow, as multiple threads will try to access simultaneously the data.\\
To improve performance, data is therefore stocked in shared block memory, taking into account the borders. This allows much faster computation afterwards. However, two things should be noted:
\begin{itemize}
	\item There is still some access conflicts with boundaries
	\item The introduction of conditions to take care of boundaries has an effect on speed.
\end{itemize}

(see Fig.~\ref{cudaKernel1})
\begin{figure}[!htbl]
\begin{Verbatim}
	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(lidX < offset && lidY < offset && lidZ < offset){
	s_sh[bindex]+=s_sh[bindex+offset]+s_sh[bindex+offset*blockDim.z]
			+s_sh[bindex+offset*blockDim.z*blockDim.y]
			+s_sh[bindex+offset*(1+blockDim.z)]
			+s_sh[bindex+offset*(1+blockDim.z*blockDim.y)]
			+s_sh[bindex+offset*blockDim.z*(1+blockDim.y)]
			+s_sh[bindex+offset*(1+blockDim.z*(1+blockDim.y))];
		}
		__syncthreads();
	}
\end{Verbatim}
\caption{Reduction of an array in shared memory, using geometry of the block to reduce synchronizations.}\label{cudaKernel1}
\end{figure}


\noindent\textbf{Note:} By default, the programs use Jacobi iteration. To use conjugate gradient, type in the option -m CG.

\section{Performance analysis}
Each program created consists of two phases: initialization (where memory is allocated and initial data filled), then iterative algorithm. The initialization part will not be taken into account when considering algorithm speed, as it is not very relevant for the computation : there are many ways of  inserting initial data, depending on how this program is combined with real problems.
\subsection*{poisson\_mpi}

\noindent What is the fraction of the non-parallelizable code of the MPI version? As the iteration is almost completely parallel, one can assume that the fraction of non-parallelizable code is around $1\%$.

\subsection*{poisson\_mpit only with pthreads}

\noindent At each iteration of the algorithm(s), pthreads are created. The thread creation process being quite expensive, it can be assumed that the non-parallelizable code is around $1\%$ as well.

\noindent Therefore for both programs, Amdahl's Law predicts an important speedup (as far as the number of processes stays small) for fixed-size problem.

\subsection*{poisson\_cuda}

\noindent Unfortunately, the cuda implementation shows only poor speed (within the order of the serial version). During debugging, it has been found that the initialization of the shared memory, with conditions and memory banking issues, was posing problem.

\noindent The memory banking issue could be solved with duplicating data.
\section{Results}

The code has been compiled on Electra and simulations have been made on a grid of size $200\times200\times200$ using Conjugate Gradient. The Jacobi version has proven to be much slower, with a very similar behavior, therefore\\
First, it can be observed on Fig.~\ref{fig:cg_single} that the MPI version behaves remarkably well : as the size is increased, the execution time is very consistent with Amdahl's Law.

\begin{figure}[h]
	\centering
	\begin{minipage}[t]{0.45\textwidth}
		\input{cg_single.tex}
		\caption{Evolution of compute time for a problem of size $N=200$ using CG method.}\label{fig:cg_single}
	\end{minipage}
	\hspace{2mm}
	\begin{minipage}[t]{0.45\textwidth}
 		\input{cg_increasing.tex}
		\caption{Evolution of compute time for a problem of increasing size.}\label{fig:cg_increasing}
	\end{minipage}
\end{figure}

To try to obtain a correlation with Gustafson's Law, the size of the problem has been gradually increased such that the total elements to be computed also increases linearly, that can be computed as
\[
	\mathrm{size}(n)=50\sqrt[3]{n}
\]
(From a starting problem of size $N=50$. The results are shown in Fig.~\ref{fig:cg_increasing}. Once again, it can be observed that MPI behaves relatively well with size augmentation : the speedup correlates pretty nicely with the predicted value of the weak scaling problem.\\

\begin{figure}[h]
	\centering
		\input{cg_mixed.tex}
		\caption{Evolution of compute time for the mixed solver poisson\_mpit, with fixed size $N=200$.}\label{fig:cg_mixed}
\end{figure}

Finally, consider the results for the mixed solver (See Fig.~\ref{fig:cg_mixed}). Once again, it can be seen that the solver behaves better with MPI scaling. Still, combining the two paradigms seems to have a noticeable effect on the speed.\\

\noindent\textbf{Note on the results}: There seem to be a problem with the behavior of pthreads on the cluster, as the speed up is relatively poor. Actually, the behavior is quite good, until 4 or 5 pthreads. This could be related to the fact that when changing the number of CPUs in the job, there is no effect whereas it should have one.\\

\noindent The code of the programs used can be found 

\newpage
\section{Conclusion}
A program has been created with the aim of exploring parallelization and optimization techniques, and, even though it shows nice results for the combination pthread/MPI, it could be further improved, by taking fully advantage of the GPU, optimizing all loops for vectorization, and by implementing better methods, as multigrid methods.

\begin{thebibliography}{10}

\bibitem{wikiCG}
{Wikipedia - Conjugate Gradient}.
\newblock \url{http://en.wikipedia.org/wiki/Conjugate_gradient_method}.

\bibitem{convExp}
Yousef Saad.
\newblock Iterative Methods for Sparse Linear Systems
\newblock 2nd Edition,
  ISBN 978-0-898715-34-7

\end{thebibliography}


\end{document}
