\documentclass{beamer}
\usepackage[]{algpseudocode}
\usepackage{tikz}
\usepackage{listings}
\definecolor{mygray}{rgb}{.99,.99,.99}
\lstset{language=C++,backgroundcolor=\color{mygray}}

\usetheme{Singapore}
\begin{document}

\title[Crisis]{Conjugate Gradient method for Poisson Solver}
\subtitle{Parallel Computing and pthreads\\[1em]\includegraphics[width=0.3\textwidth]{EPFL_LOG_QUADRI_Red.eps}
}
\author
{Etienne Favre}
\frame{\titlepage}

\begin{frame}
	\frametitle{Problem definition}
	\begin{itemize}
		\item Consider $3D$ domain $B=]0,1[^3$.
		\item  Find $u:\mathbb{R}^3\longrightarrow\mathbb{R}$ satisfying
	\[
	\begin{cases}
		\Delta u=f &f\;\mathrm{on}\;B\\
		u=g &g\,\mathrm{on}\;\partial B
	\end{cases}
	\]
	With $f,g:\mathbb{R}^3\longrightarrow\mathbb{R}$.
	
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Finite-differences discretization}
	Using standard finite-differences centered discretization
\[
	\frac{\partial u}{\partial x_i}(x)\simeq\frac{u(x-he_i)-2u(x)+u(x+he_i)}{h^2}
\]
		Defining
		\[
			u_{i\,N^2+j\,N+k} = u(hi\,e_1+,hj\,e_2+hk\,e_3)
		\]
		Problem can be expressed as
\[
	Au=b
\]
	Where
	\begin{itemize}
		\item $N$ is the mesh size
		\item $h=\frac{1}{N-1}$
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Matrix structure - Zoom on diagonal}
	
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{matrix_structure}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Conjugate Gradient Method}
	\begin{algorithmic}
\State $r^{(0)}=b-Au^{(0)},p^{(0)}=r_0,k=0$
\While{not converged}
	\State $\alpha_k=\frac{r^{(k)\,*r^{(k)}}}{p^{(k)\,*}Ap^{(k)}}$
	\State $u^{(k+1)}=u^{(k)}+\alpha_kp^{(k)}$
	\State $r^{(k+1)}=r^{(k)}-\alpha_kAp^{(k)}$
	\If{$r^{(k+1)}<\mathrm{tol}$}
		\State stop
	\Else
		\State$\beta_k=\frac{r^{(k+1)\,*}r^{(k+1)}}{r^{(k)\,*}r^{(k)}}$
		\State $p^{(k+1)}=r^{(k+1)}+\beta_kp^{(k)}$
		\State$k+=1$
	\EndIf
\EndWhile
\end{algorithmic}
\end{frame}

\begin{frame}
	\frametitle{Conjugate Gradient Method}
	\begin{itemize}
		\item Iterative algorithm
		\item Requires matrix multiplication and vector product
		\item<2-> Suitable for parallelization
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Program basics}
	\begin{itemize}
		\item C++
		\item Oriented-object programming : polymorphism, class variables
		\item Allows for flexible and modulable programming
		\item<2-> Dynamic memory allocation :
		\begin{itemize}
			\item method
			\item number of threads
			\item mesh size
			\item number of MPI processes
		\end{itemize}
		\item<2-> mpirun -np 5 ./poisson\_mpit -m CG -s 250 -n 4 -o output\_file
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Parallelize using MPI}
	Computation can be massively speeded up, taking care of :
	\begin{itemize}
		\item Synchronization of computation
		\item Boundary exchange
	\end{itemize}
	
\begin{figure}\label{fMPIcomm}
\centering
\begin{tikzpicture}
\draw (10.5,0) -- (0,0) -- (0,3) -- (10.5,3) -- cycle;
\draw[blue] (10,2.5) -- (10,0.5) -- (5.5,0.5) -- (5.5,2.5) -- cycle;
\draw[blue] (0.5,2.5) -- (0.5,0.5) -- (5,0.5) -- (5,2.5) -- cycle;
\draw[step=0.5,black,opacity=0.1] (0,0) grid(10.5,3);
\draw[blue] (2.5,1.5) node[align=center]{Interior 1};
\draw[blue] (8,1.5) node[align=center]{Interior 2};

\draw[red,opacity=0.5] (4.8,-0.2) -- (5.7,-0.2) -- (5.7,3.2) -- (4.8,3.2) -- cycle;
\draw[red] (5.25,3.4) node{Shared vertices};
\draw[green,opacity=0.8,dashed, thick] (0,0) -- (5.5,0) -- (5.5,3) -- (0,3) -- cycle;
\draw[purple,dashed, thick] (10.5,0) -- (5,0) -- (5,3) -- (10.5,3) -- cycle;
\end{tikzpicture}
$2D$ grid schematics of MPI memory sharing. The vertices represent the computation points
\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Parallelize using MPI}
	\begin{itemize}
		\item Data stored in a $1D$ array
		\item<2-> Optimal with MPI functions
		\item<2-> To minimize data transfer, split cube into one direction :
		\item<3-> Minimizes the calls to send/receive functions
	\end{itemize}

	\begin{figure}
	\begin{lstlisting}[frame=single]
	if(m_neighborMin!=-1){
	  MPI_Irecv(m_p,sX,_MPI_DATATYPE,m_neighborMin,
	    0,MPI_COMM_WORLD,request+l++);
	  MPI_Isend(m_p+sX,sX,_MPI_DATATYPE,m_neighborMin,
	    1,MPI_COMM_WORLD,request+l++);
	}
	\end{lstlisting}
	Data sending to another process
	\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Parallelize using MPI}
	\begin{itemize}
		\item Use time where data is exchanged to compute boundary-independent parts
		\item For dot products, compute locally each part of the sum and put together using MPI\_Allreduce
	\end{itemize}
	
	\begin{figure}
	\begin{lstlisting}[frame=single]
	// Collect global (p,Ap)
	P_REAL gpAp(0.0);
	MPI_Allreduce ( &pAp, &gpAp, 1, _MPI_DATATYPE,
	  MPI_SUM,MPI_COMM_WORLD );
	\end{lstlisting}
	Data sending to another process
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Parallelize using pthreads}
	Inside each MPI process, Optimize further computation
	\begin{itemize}
		\item pthreads : lighter than whole process
		\item<2-> shared memory : no boundary exchange
		\item<3-> need for a mechanism of synchronization
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Parallelize using pthreads}
	\begin{itemize}
		\item Domain is now divided in $z$-direction
		\item Synchronization of threads : block with condition variable
		\begin{lstlisting}[frame=single]
	// Wait for boundary data
	pthread_mutex_lock(&m_mutex);
	if(m_blockThreads){
	    pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);
	\end{lstlisting}
		\item In main thread, unblock when needed
		\begin{lstlisting}[frame=single]
	// Broadcast signal ready
	m_blockThreads = false;
	pthread_cond_broadcast(&m_cond);
	\end{lstlisting}
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Parallelize using pthreads}
	\begin{itemize}
		\item Subdivide the domain in roughly equal parts to minimize the time difference for each thread
		\item Condition variables allow to avoid multiple sets of thread creation
	\end{itemize}
\end{frame}


\begin{frame}[fragile]
	\frametitle{Parallelize using pthreads}
		\begin{itemize}
			\item Wait for all threads mechanism
		\end{itemize}
		\begin{lstlisting}[frame=single]
	pthread_mutex_lock(&m_mutex);
	m_pAp+=pAp;
	m_finishedThreads++;
	if(m_finishedThreads==m_numThreads){
	    // Do MPI reduction
	    // Release all threads
	    m_finishedThreads = 0;
	    pthread_cond_broadcast(&m_cond);
	}
	else{
	    pthread_cond_wait(&m_cond,&m_mutex);
	}
	pthread_mutex_unlock(&m_mutex);
	\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{Performance analysis}
		\begin{itemize}
			\item Fraction of non-parallelizable code for MPI pure : $\alpha\simeq$1\%
			\item Using pthreads : can assume $\alpha\simeq1$\% as well
			\item<2-> Amdahl's Law :
			Given $p$ the number of processors and $\alpha$, speedup given by
			\[
				S_p=\frac{1}{\alpha+\frac{1-\alpha}{p}}
			\]
			\item<3-> Gustafson's Law : 
			\[
				\tilde{S}_p=p-\alpha(p-1)
			\]
		\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Results - Strong scaling}
	
	\begin{itemize}
		\item Compiled on Electra
		\item $N=200$ ($200\times200\times200$ mesh)
		\item First, study behavior separately
	\end{itemize}

\begin{figure}[h]
	\centering
		\input{cg_single.tex}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Results - Weak scaling}
	
	\begin{itemize}
		\item $N=50\sqrt[3]{n}$ ($n$ number of processes)
	\end{itemize}

\begin{figure}[h]
	\centering
		\input{cg_increasing.tex}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Results - Mixing together (Strong scaling)}
	
	\begin{itemize}
		\item $N=200$ ($200\times200\times200$ mesh)
	\end{itemize}

\begin{figure}[h]
	\centering
		\includegraphics[width=\textwidth]{cg_mixed_standalone}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Conclusion}
	\begin{itemize}
		\item MPI works very well, parallelization on point
		\item Coupling with pthreads way less efficient on cluster
		\item<2-> Still, combined paradigms provides good speedup effect, until more than 4 pthreads
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Room for improvement}
	\begin{itemize}
		\item For PDE discretizations, matrices are sparse : implement a sparse solver would be more optimized
		\item<2-> better usage of pthreads on cluster
	\end{itemize}
\end{frame}


\end{document}