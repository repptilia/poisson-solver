# Makefile for PPaP project
# Etienne Favre

CUDA_LIB_PATH = /usr/local/cuda/lib64
CUDA_INC_PATH = /usr/local/cuda/include
# MPI Location
# On Electra
#MPI_HOME      = /usr/local/apps/openmpi/1.4.3/gcc-4.1.2/
# On siepc
MPI_HOME = /usr/lib/openmpi
CC=mpicxx
CCU=nvcc
CUFLAGS = -c -O3 -I$(CUDA_INC_PATH) -I$(MPI_HOME)/include
# -O3
CFLAGS  = -c -pthread -O3 -msse2
#-msse2 -ftree-vectorizer-verbose=2
LDFLAGS = -L$(CUDA_LIB_PATH) -lcudart -lcuda -L$(MPI_HOME)/lib -lmpi -lmpi_cxx
SOURCES = PoissonProblem.cpp PoissonSolverJacobi.cpp PoissonSolverCG.cpp ProblemMPI.cpp PoissonSolverJacobiMPI.cpp PoissonSolverCGMPI.cpp PoissonSolverJacobiMPIT.cpp PoissonSolverCGMPIT.cpp
CUSRC   = PoissonSolverJacobiC.cu PoissonSolverCGC.cu PoissonSolverJacobiCMPI.cu PoissonSolverCGCMPI.cu
EX_SRC  = poisson_serial.cpp poisson_mpi.cpp poisson_mpit.cpp
EXU_SRC = poisson_cuda.cu poisson_cudampi.cu
# poisson_all.cu
OBJECTS = $(SOURCES:.cpp=.o)
UOBJECTS= $(CUSRC:.cu=.o)
EXECUTS = $(EX_SRC:.cpp=) $(EXU_SRC:.cu=)

all: $(SOURCES) $(EX_SRC) $(EXECUTS)

poisson_serial: $(OBJECTS) poisson_serial.o
	$(CC) $(LDLAGS) $(OBJECTS) poisson_serial.o -o $@

poisson_mpi: $(OBJECTS) poisson_mpi.o
	$(CC) $(LDLAGS) $(OBJECTS) poisson_mpi.o -o $@

poisson_mpit: $(OBJECTS) poisson_mpit.o
	$(CC) $(LDLAGS) $(OBJECTS) poisson_mpit.o -o $@

poisson_cuda: $(OBJECTS) $(UOBJECTS) poisson_cuda.o
	$(CCU) $(LDFLAGS) $(UOBJECTS) $(OBJECTS) poisson_cuda.o -o $@

poisson_cudampi: $(OBJECTS) $(UOBJECTS) poisson_cudampi.o
	$(CCU) $(LDFLAGS) $(UOBJECTS) $(OBJECTS) poisson_cudampi.o -o $@

poisson_all: $(OBJECTS) $(UOBJECTS) poisson_all.o
	$(CCU) $(LDFLAGS) $(UOBJECTS) $(OBJECTS) poisson_all.o -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

%.o : %.cu
	$(CCU) $(CUFLAGS) $< -o $@

nocuda: poisson_serial poisson_mpi poisson_mpit

clean:
	rm -rf *.o poisson_mpit poisson_serial poisson_mpi poisson_cuda poisson_cudampi poisson_all

rebuild: clean all
