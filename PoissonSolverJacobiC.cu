#include "PoissonSolverJacobiC.hpp"

// Computes new data and previous error
__global__ void update(const P_REAL* data, P_REAL* tmp, const P_REAL* f, P_REAL* reduced, const unsigned int sizeX,const unsigned int sizeY,const unsigned int sizeZ, const P_REAL aX, const P_REAL aY, const P_REAL aZ, const P_REAL aF, const P_REAL cX, const P_REAL cY, const P_REAL cZ){

	// Global ID
#if _GRID_DIM == DDD
	int idX(blockDim.x*blockIdx.x+threadIdx.x);
	int idY(blockDim.y*blockIdx.y+threadIdx.y);
	int idZ(blockDim.z*blockIdx.z+threadIdx.z);
#elif _GRID_DIM == DD
	int tt(blockIdx.x%(gridDim.x*gridDim.y));
	int idX(blockDim.x*blockIdx.x+threadIdx.x);
	int idY(blockDim.y*(tt+threadIdx.y);
	int idZ(blockDim.z*((blockIdx.x-tt)/(gridDim.x*gridDim.y)+threadIdx.z);
#endif

	// Local ID
	int lidX(threadIdx.x);
	int lidY(threadIdx.y);
	int lidZ(threadIdx.z);


	const unsigned int sX(sizeY*sizeZ);

	__shared__ P_REAL s_sh[BLOCK_SIZE*BLOCK_SIZE*BLOCK_SIZE];

	__shared__ P_REAL s_data[BLOCK_SIZE+2][BLOCK_SIZE+2][BLOCK_SIZE+2];

	// Index in array (shifted of 1 in each coordinate, as we want to avoid the boundary
	unsigned int index(((idX+1)*sizeY+idY+1)*sizeZ+idZ+1) ;
	unsigned int bindex((lidX*BLOCK_SIZE+lidY)*BLOCK_SIZE+lidZ);

	// Take care of array boundaries
	if(lidX==0){
		s_data[0][lidY+1][lidZ+1]=data[index-sX];
		s_data[BLOCK_SIZE+1][lidY+1][lidZ+1]=data[index+BLOCK_SIZE*sX];
	}
	if(lidY==0){
		s_data[lidX+1][0][lidZ+1]=data[index-sizeZ];
		s_data[lidX+1][BLOCK_SIZE+1][lidZ+1]=data[index+BLOCK_SIZE*sizeZ];
	}
	if(lidZ==0){
		s_data[lidX+1][lidY+1][0]=data[index-1];
		s_data[lidX+1][lidY+1][BLOCK_SIZE+1]=data[index+BLOCK_SIZE];
	}
	// Fill in interior
	s_data[lidX+1][lidY+1][lidZ+1]=data[index];

	__syncthreads();

	tmp[index] = aX*(s_data[lidX][lidY+1][lidZ+1]+s_data[lidX+2][lidY+1][lidZ+1])
		+aY*(s_data[lidX+1][lidY][lidZ+1]+s_data[lidX+1][lidY+2][lidZ+1])
		+aZ*(s_data[lidX+1][lidY+1][lidZ]+s_data[lidX+1][lidY+1][lidZ+2])
		-aF*f[index];

	P_REAL vtmp(cX*(s_data[lidX][lidY+1][lidZ+1]+s_data[lidX+2][lidY+1][lidZ+1])
		+cY*(s_data[lidX+1][lidY][lidZ+1]+s_data[lidX+1][lidY+2][lidZ+1])
		+cZ*(s_data[lidX+1][lidY+1][lidZ]+s_data[lidX+1][lidY+1][lidZ+2])
		-2.0f*(cX+cY+cZ)*s_data[lidX+1][lidY+1][lidZ+1]-f[index]);

	s_sh[bindex] = vtmp*vtmp;

	// Wait for all threads
	__syncthreads();

	// Take advantage of the geometry of the block
	// At each iteration, there are 7 points to consider
	// Use byte-shift to ease division by 2

	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(lidX < offset && lidY < offset && lidZ < offset){
			s_sh[bindex]+=s_sh[bindex+offset]+s_sh[bindex+offset*blockDim.z]+s_sh[bindex+offset*blockDim.z*blockDim.y]
					+s_sh[bindex+offset*(1+blockDim.z)]
					+s_sh[bindex+offset*(1+blockDim.z*blockDim.y)]
					+s_sh[bindex+offset*blockDim.z*(1+blockDim.y)]
					+s_sh[bindex+offset*(1+blockDim.z*(1+blockDim.y))];
		}

		// Wait for all threads
		__syncthreads();
	}

	// Write final result with (0,0,0) thread
	if(threadIdx.x==0 && threadIdx.y==0 && threadIdx.z == 0){
#if _GRID_DIM == DDD
		reduced[(blockIdx.x*gridDim.y+blockIdx.y)*gridDim.z+blockIdx.z] = s_sh[0];
#elif _GRID_DIM == DD
		reduced[blockIdx.y*gridDim.y*gridDim.z+blockIdx.x]=s_sh[0];
#endif
	}
}

PoissonSolverJacobiC::PoissonSolverJacobiC(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, int device):PoissonSolverJacobi(sizeX,sizeY,sizeZ){

	// Create grids
	m_blockSize = dim3(BLOCK_SIZE,BLOCK_SIZE,BLOCK_SIZE);
#if _GRID_DIM == DDD
	m_gridSize  = dim3((m_sizeX-2)/m_blockSize.x,(m_sizeY-2)/m_blockSize.y,(m_sizeZ-2)/m_blockSize.z);
#elif _GRID_DIM == DD
	m_gridSize = dim3((m_sizeX-2)/m_blockSize.x,(m_sizeY-2)/m_blockSize.y*(m_sizeZ-2)/m_blockSize.z,1);
#endif
	// Completely re-create constructor, as what we need this time is GPU-side variables mostly
	// Set CUDA device
	cudaSetDevice(device);

	// Convert to size_t
	size_t t((size_t)sizeX*(size_t)sizeY*(size_t)sizeZ);

	cudaMalloc((void**)&d_data,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_tmp,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_func,t*sizeof(P_REAL));

	// Initialize memory
	cudaMemset(d_data,0,t*sizeof(P_REAL));
	cudaMemset(d_tmp,0,t*sizeof(P_REAL));
	cudaMemset(d_func,0,t*sizeof(P_REAL));

	m_maxIt = (size_t)sizeZ*((size_t)sizeY*((size_t)sizeX-2)-2)-2;

	size_t grid(m_gridSize.x*m_gridSize.y*m_gridSize.z);
	m_reduced = (P_REAL*)malloc(grid*sizeof(P_REAL));
	cudaMalloc((void**)&d_reduced,grid*sizeof(P_REAL));
	cudaMemset(d_reduced,0,grid*sizeof(P_REAL));
}

void PoissonSolverJacobiC::set(){
	PoissonProblem::set();
	P_REAL sum(m_delX*m_delX*m_delY*m_delY+m_delY*m_delY*m_delZ*m_delZ+m_delZ*m_delZ*m_delX*m_delX);
	sum *=2.0;
	m_j_ax = m_delY*m_delY*m_delZ*m_delZ/sum;
	m_j_ay = m_delX*m_delX*m_delZ*m_delZ/sum;
	m_j_az = m_delX*m_delX*m_delY*m_delY/sum;
	m_j_af = m_j_ax*m_delX*m_delX;

	// Convert to size_t
	size_t t((size_t)m_sizeX*(size_t)m_sizeY*(size_t)m_sizeZ);

	m_cX = 1.0f/m_delX;
	m_cY = 1.0f/m_delY;
	m_cZ = 1.0f/m_delZ;
	m_err = 0.0f;

	// Transfer data to GPU
	cudaMemcpy(d_data, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tmp, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_func, m_func, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
}

void PoissonSolverJacobiC::iterate(){
	// Number of blocks
	size_t numBlocks(m_gridSize.x*m_gridSize.y*m_gridSize.z);
	// Size of blocks
	//size_t sizeBlock(m_blockSize.x*m_blockSize.y*m_blockSize.z);

	/**
	 * NOTE: on shared memory allocation
	 *
	 * To fasten computation, we load partially the "shifted" versions of data on the shared memory.
	 * Therefore, we need to allocate memory for:
	 * -d_data +- 1
	 * -d_data +- m_sizeZ
 	 * -d_data +- m_sizeZ*m_sizeY
	 *
	 * This suggests an allocation of 6*sizeBlock for the whole array
	 * +1 for boundary on top
	 * +1 for error = 7
	 **/

	// Compute on the GPU
	update<<<m_gridSize,m_blockSize>>>(d_data,d_tmp,d_func,d_reduced,m_sizeX,m_sizeY,m_sizeZ,m_j_ax,m_j_ay,m_j_az,m_j_af,m_cX,m_cY,m_cZ);

	// Pull back error
	cudaMemcpy(m_reduced, d_reduced, numBlocks*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	m_err = 0.0f;
	for(unsigned int i(0);i<numBlocks;i++){
		m_err+=m_reduced[i];
	}

	// Swap memories
	P_REAL* swap(NULL);
	swap=d_data;
	d_data=d_tmp;
	d_tmp=swap;
}


void PoissonSolverJacobiC::printData(std::ostream& stream){
	// Pull data back
	cudaMemcpy(m_data, d_data, m_sizeX*m_sizeY*m_sizeZ*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				stream << m_data[b+k] << " ";
			}
			stream << std::endl;
			b+=m_sizeZ;
		}
		stream << std::endl;
	}
}

PoissonSolverJacobiC::~PoissonSolverJacobiC(){
	cudaFree(d_data);
	cudaFree(d_tmp);
	cudaFree(d_func);
	cudaFree(d_reduced);
}

