#include "PoissonSolverCGC.hpp"

// Computes new Ap, and (p,Ap) to a reduced sum
__global__ void updateAp(P_REAL* Ap, const P_REAL* __restrict__ p, P_REAL* reduced, const unsigned int sizeX,const unsigned int sizeY,const unsigned int sizeZ, const P_REAL cX, const P_REAL cY, const P_REAL cZ){
	// For storing (p,Ap)
	extern __shared__ P_REAL s_sh[];

	P_REAL* s_d(&s_sh[blockDim.x*blockDim.y*blockDim.z]);

#if _GRID_DIM == DDD
	// Get grid size
	unsigned int gY((sizeY-2)/blockDim.y),gZ((sizeZ-2)/blockDim.z);

	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*blockIdx.y+threadIdx.y+1);
	unsigned int idZ(blockDim.z*blockIdx.z+threadIdx.z+1);
#elif _GRID_DIM == DD
	unsigned int gX((sizeX-2)/blockDim.x),gY((sizeY-2)/blockDim.y);

	unsigned int tt(blockIdx.x%(gX*gY));
	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*(tt+threadIdx.y+1);
	unsigned int idZ(blockDim.z*((blockIdx.x-tt)/(gX*gY)+threadIdx.z+1);
#endif

	//TODO: Check that the index is within range (wrt how the blocks are constructed)
	// Not needed in current fashion as blocks are arranged st its ok

	unsigned int sX(sizeY*sizeZ);
	unsigned int i((idX*sizeY+idY)*sizeZ+idZ);
	unsigned int bi((threadIdx.x*blockDim.y+threadIdx.y)*blockDim.z+threadIdx.z);

	// On thread bi, we write to the shared memory where other threads would use that information.
	// This way, data[i] is only accessed by a specific thread.
	P_REAL dat(p[i]);

	// Lower boundary will be overwritten anyway by error
	// to -z neighbor:
	s_d[6*(bi-blockDim.z*blockDim.y)]=dat;
	// to -y neighbor:
	s_d[6*(bi-blockDim.z)+2]=dat;
	// to -x neighbor:
	s_d[6*(bi-1)+4]=dat;

	// Upper boundary is allocated
	// to +z neighbor:
	s_d[6*(bi+blockDim.z*blockDim.y)+1]=dat;
	// to +y neighbor:
	s_d[6*(bi+blockDim.z)+3]=dat;
	// to +z neighbor:
	s_d[6*(bi+1)+5]=dat;

	__syncthreads();
	// Take care of boundary effects
	if(threadIdx.x==0){
		s_d[6*bi+1]=p[i-sX];
	}
	if(threadIdx.x==blockDim.x-1){
		s_d[6*bi]=p[i+sX];
	}
	if(threadIdx.y==0){
		s_d[6*bi+3]=p[i-sizeZ];
	}
	if(threadIdx.y==blockDim.y-1){
		s_d[6*bi+2]=p[i+sizeZ];
	}
	if(threadIdx.z==0){
		s_d[6*bi+5]=p[i-1];
	}
	if(threadIdx.z==blockDim.z-1){
		s_d[6*bi+4]=p[i+1];
	}

	__syncthreads();

	Ap[i] = (cX*(s_d[6*bi]+s_d[6*bi+1])
		   +cY*(s_d[6*bi+2]+s_d[6*bi+3])
		   +cZ*(s_d[6*bi+4]+s_d[6*bi+5])
		   -2.0f*(cX+cY+cZ)*dat);

	s_sh[bi]=Ap[i]*p[i];

	// Wait for all threads
	__syncthreads();

	// Take advantage of the geometry of the block
	// At each iteration, there are 7 points to consider
	// Use byte-shift to ease division by 2

	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(threadIdx.x < offset && threadIdx.y < offset && threadIdx.z < offset){
			s_sh[bi]+=s_sh[bi+offset]+s_sh[bi+offset*blockDim.z]+s_sh[bi+offset*blockDim.z*blockDim.y]
					+s_sh[bi+offset*(1+blockDim.z)]
					+s_sh[bi+offset*(1+blockDim.z*blockDim.y)]
					+s_sh[bi+offset*blockDim.z*(1+blockDim.y)]
					+s_sh[bi+offset*(1+blockDim.z*(1+blockDim.y))];
		}

		// Wait for all threads
		__syncthreads();
	}									

	// Write final result with (0,0,0) thread
	if(threadIdx.x==0 && threadIdx.y==0 && threadIdx.z == 0){
		//TODO:Change this when general formula
#if _GRID_DIM == DDD
		reduced[(blockIdx.x*gY+blockIdx.y)*gZ+blockIdx.z] = s_sh[0];
#elif _GRID_DIM == DD
		reduced[blockIdx.y*gY*gZ+blockIdx.x]=s_sh[0];
#endif
	}
}

// Computes m_data,m_r and new residual to reduced sum
__global__ void updateDRR(P_REAL* p, P_REAL* Ap, P_REAL* r, P_REAL* data, P_REAL* reduced, P_REAL alpha,const unsigned int sizeY,const unsigned int sizeZ){
	// For storing (p,Ap)
	extern __shared__ P_REAL s_sh[];

#if _GRID_DIM == DDD
	// Get grid size
	unsigned int gY((sizeY-2)/blockDim.y),gZ((sizeZ-2)/blockDim.z);


	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*blockIdx.y+threadIdx.y+1);
	unsigned int idZ(blockDim.z*blockIdx.z+threadIdx.z+1);
#elif _GRID_DIM == DD
	unsigned int gX((sizeX-2)/blockDim.x),gY((sizeY-2)/blockDim.y);

	unsigned int tt(blockIdx.x%(gX*gY));
	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*(tt+threadIdx.y+1);
	unsigned int idZ(blockDim.z*((blockIdx.x-tt)/(gX*gY)+threadIdx.z+1);
#endif

	//TODO: Check that the index is within range (wrt how the blocks are constructed)
	// Not needed in current fashion as blocks are arranged st its ok

	unsigned int i((idX*sizeY+idY)*sizeZ+idZ);
	unsigned int bi((threadIdx.x*blockDim.y+threadIdx.y)*blockDim.z+threadIdx.z);

	data[i] += alpha * p[i];
	r[i] -= alpha * Ap[i];
	s_sh[bi] = r[i] * r[i];

	// Wait for all threads
	__syncthreads();

	// Take advantage of the geometry of the block
	// At each iteration, there are 7 points to consider
	// Use byte-shift to ease division by 2

	for(int offset = blockDim.x/2;offset>0;offset>>=1){
		if(threadIdx.x < offset && threadIdx.y < offset && threadIdx.z < offset){
			s_sh[bi]+=s_sh[bi+offset]+s_sh[bi+offset*blockDim.z]+s_sh[bi+offset*blockDim.z*blockDim.y]
					+s_sh[bi+offset*(1+blockDim.z)]
					+s_sh[bi+offset*(1+blockDim.z*blockDim.y)]
					+s_sh[bi+offset*blockDim.z*(1+blockDim.y)]
					+s_sh[bi+offset*(1+blockDim.z*(1+blockDim.y))];
		}

		// Wait for all threads
		__syncthreads();
	}

	// Write final result with (0,0,0) thread
	if(threadIdx.x==0 && threadIdx.y==0 && threadIdx.z == 0){
		//TODO:Change this when general formula
#if _GRID_DIM == DDD
		reduced[(blockIdx.x*gY+blockIdx.y)*gZ+blockIdx.z] = s_sh[0];
#elif _GRID_DIM == DD
		reduced[blockIdx.y*gY*gZ+blockIdx.x]=s_sh[0];
#endif
	}
}

// Update p
__global__ void updateP(P_REAL* p, P_REAL* r, P_REAL beta,const unsigned int sizeY,const unsigned int sizeZ){

#if _GRID_DIM == DDD
	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*blockIdx.y+threadIdx.y+1);
	unsigned int idZ(blockDim.z*blockIdx.z+threadIdx.z+1);
#elif _GRID_DIM == DD
	unsigned int gX((sizeX-2)/blockDim.x),gY((sizeY-2)/blockDim.y);

	unsigned int tt(blockIdx.x%(gX*gY));
	unsigned int idX(blockDim.x*blockIdx.x+threadIdx.x+1);
	unsigned int idY(blockDim.y*(tt+threadIdx.y+1);
	unsigned int idZ(blockDim.z*((blockIdx.x-tt)/(gX*gY)+threadIdx.z+1);
#endif

	//TODO: Check that the index is within range (wrt how the blocks are constructed)
	// Not needed in current fashion as blocks are arranged st its ok

	unsigned int i((idX*sizeY+idY)*sizeZ+idZ);

	p[i] = r[i] + beta * p[i];
}

PoissonSolverCGC::PoissonSolverCGC(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ,P_REAL tol, int device):PoissonSolverCG(sizeX,sizeY,sizeZ,tol){
	// Create grids
	m_blockSize = dim3(BLOCK_SIZE,BLOCK_SIZE,BLOCK_SIZE);
#if _GRID_DIM == DDD
	m_gridSize  = dim3((m_sizeX-2)/m_blockSize.x,(m_sizeY-2)/m_blockSize.y,(m_sizeZ-2)/m_blockSize.z);
#elif _GRID_DIM == DD
	m_gridSize = dim3((m_sizeX-2)/m_blockSize.x,(m_sizeY-2)/m_blockSize.y*(m_sizeZ-2)/m_blockSize.z,1);
#endif
	// Completely re-create constructor, as what we need this time is GPU-side variables mostly
	// Set CUDA device
	cudaSetDevice(device);

	// Convert to size_t
	size_t t((size_t)sizeX*(size_t)sizeY*(size_t)sizeZ);

	cudaMalloc((void**)&d_data,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_Ap,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_p,t*sizeof(P_REAL));
	cudaMalloc((void**)&d_r,t*sizeof(P_REAL));

	// Initialize memory
	cudaMemset(d_data,0,t*sizeof(P_REAL));
	cudaMemset(d_Ap,0,t*sizeof(P_REAL));
	cudaMemset(d_p,0,t*sizeof(P_REAL));
	cudaMemset(d_r,0,t*sizeof(P_REAL));

	m_maxIt = (size_t)sizeZ*((size_t)sizeY*((size_t)sizeX-2)-2)-2;

	size_t grid(m_gridSize.x*m_gridSize.y*m_gridSize.z);
	m_reduced = (P_REAL*)malloc(grid*sizeof(P_REAL));
	cudaMalloc((void**)&d_reduced,grid*sizeof(P_REAL));
	cudaMemset(d_reduced,0,grid*sizeof(P_REAL));
}

void PoissonSolverCGC::set(){
	// Convert to size_t
	size_t t((size_t)m_sizeX*(size_t)m_sizeY*(size_t)m_sizeZ);
	// Temporarily create on memory
	P_REAL* tmp = (P_REAL*)calloc(t,sizeof(P_REAL));

	m_cX = 1.0f/m_delX;
	m_cY = 1.0f/m_delY;
	m_cZ = 1.0f/m_delZ;
	m_normr = 0.0f;
	const unsigned int sX(m_sizeZ*m_sizeY);
	unsigned int b(sX+m_sizeZ);

	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				tmp[b+k]=m_func[b+k]-m_cX*(m_data[b+k-sX]+m_data[b+k+sX]-2.0f*m_data[b+k])
						    -m_cY*(m_data[b+k-m_sizeZ]+m_data[b+k+m_sizeZ]-2.0f*m_data[b+k])
						    -m_cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0f*m_data[b+k]);
				m_normr+=tmp[b+k]*tmp[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}

	// Transfer to device
	cudaMemcpy(d_r, tmp, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	cudaMemcpy(d_p, tmp, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
	// Transfer data as well
	cudaMemcpy(d_data, m_data, t*sizeof(P_REAL), cudaMemcpyHostToDevice);
}

void PoissonSolverCGC::iterate(){
	// Number of blocks
	size_t numBlocks(m_gridSize.x*m_gridSize.y*m_gridSize.z);
	// Size of blocks
	size_t sizeBlock(m_blockSize.x*m_blockSize.y*m_blockSize.z);
	// Compute Ap and (p,Ap) on the GPU
	updateAp<<<m_gridSize,m_blockSize,7*sizeBlock*sizeof(P_REAL)>>>(d_Ap,d_p,d_reduced,m_sizeX,m_sizeY,m_sizeZ,m_cX,m_cY,m_cZ);

	// Pull back (p,Ap)
	cudaMemcpy(m_reduced, d_reduced, numBlocks*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	P_REAL pAp(0.0f);
	for(unsigned int i(0);i<numBlocks;i++){
		pAp+=m_reduced[i];
	}

	// Compute the coefficient alpha
	P_REAL alpha(m_normr/pAp);

	// Update x,r and compute the new residual norm
	updateDRR<<<m_gridSize,m_blockSize,sizeBlock*sizeof(P_REAL)>>>(d_p,d_Ap,d_r,d_data,d_reduced,alpha,m_sizeY,m_sizeZ);

	// Pull back rNew
	cudaMemcpy(m_reduced, d_reduced, numBlocks*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	P_REAL rNew(0.0f);
	for(unsigned int i(0);i<numBlocks;i++){
		rNew+=m_reduced[i];
	}
	// If convergence reached, stop
	if(rNew<m_tol){
		m_normr=rNew;
		return;
	}
	// Update p, residual norm if convergence not reached
	P_REAL beta(rNew/m_normr);

	updateP<<<m_gridSize,m_blockSize>>>(d_p,d_r,beta,m_sizeY,m_sizeZ);

	m_normr=rNew;
}


void PoissonSolverCGC::printData(std::ostream& stream){
	// Pull data back
	cudaMemcpy(m_data, d_data, m_sizeX*m_sizeY*m_sizeZ*sizeof(P_REAL), cudaMemcpyDeviceToHost);

	unsigned int b(0);
	for(unsigned int i(0);i<m_sizeX;i++){
		for(unsigned int j(0);j<m_sizeY;j++){		
			for(unsigned int k(0);k<m_sizeZ;k++){
				stream << m_data[b+k] << " ";
			}
			stream << std::endl;
			b+=m_sizeZ;
		}
		stream << std::endl;
	}
}

PoissonSolverCGC::~PoissonSolverCGC(){
	cudaFree(d_data);
	cudaFree(d_Ap);
	cudaFree(d_p);
	cudaFree(d_r);
	cudaFree(d_reduced);
}

