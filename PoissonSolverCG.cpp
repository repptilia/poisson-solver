#include "PoissonSolverCG.hpp"

PoissonSolverCG::PoissonSolverCG(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, P_REAL tol) : PoissonProblem(sizeX,sizeY,sizeZ),m_tol(tol){
	size_t t(sizeX*sizeY*sizeZ);
	m_r = (P_REAL*)calloc(t,sizeof(P_REAL));
	m_p = (P_REAL*)calloc(t,sizeof(P_REAL));
	m_Ap = (P_REAL*)calloc(t,sizeof(P_REAL));
}

void PoissonSolverCG::set(){
	m_cX = 1.0/m_delX;
	m_cY = 1.0/m_delY;
	m_cZ = 1.0/m_delZ;
	m_normr = 0.0;
	const unsigned int sX(m_sizeZ*m_sizeY);
	unsigned int b(sX+m_sizeZ);

	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_r[b+k]=m_func[b+k]-m_cX*(m_data[b+k-sX]+m_data[b+k+sX]-2.0*m_data[b+k])
						    -m_cY*(m_data[b+k-m_sizeZ]+m_data[b+k+m_sizeZ]-2.0*m_data[b+k])
						    -m_cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0*m_data[b+k]);
				m_p[b+k]=m_r[b+k];
				m_normr+=m_r[b+k]*m_r[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
}

void PoissonSolverCG::iterate(){
	// Compute Ap and (p,Ap) in the same time
	P_REAL pAp(0.0);
	unsigned int sX(m_sizeY*m_sizeZ);
	unsigned int b(sX+m_sizeZ);
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_Ap[b+k]=m_cX*(m_p[b+k-sX]+m_p[b+k+sX]-2.0*m_p[b+k])
					 +m_cY*(m_p[b+k-m_sizeZ]+m_p[b+k+m_sizeZ]-2.0*m_p[b+k])
					 +m_cZ*(m_p[b+k-1]+m_p[b+k+1]-2.0*m_p[b+k]);
				pAp+=m_p[b+k]*m_Ap[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Compute the coefficient alpha
	P_REAL alpha(m_normr/pAp);
	// Update x,r and compute the new residual norm
	P_REAL rNew(0.0);
	b=sX+m_sizeZ;
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_data[b+k]+=alpha*m_p[b+k];
				m_r[b+k]-=alpha*m_Ap[b+k];
				rNew+=m_r[b+k]*m_r[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// If convergence reached, stop
	if(rNew<m_tol){
		m_normr=rNew;
		return;
	}
	// Update p, residual norm if convergence not reached
	P_REAL beta(rNew/m_normr);
	b=sX+m_sizeZ;
	for(unsigned int i(1);i<m_sizeX-1;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){		
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_p[b+k]=m_r[b+k]+beta*m_p[b+k];
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	m_normr=rNew;
}

