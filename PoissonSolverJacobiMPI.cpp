#include "PoissonSolverJacobiMPI.hpp"

PoissonSolverJacobiMPI::PoissonSolverJacobiMPI(unsigned int sizeX,unsigned int sizeY, unsigned int sizeZ, int rank, int neighborMin, int neighborMax): PoissonSolverJacobi(sizeX,sizeY,sizeZ), ProblemMPI(rank,neighborMin,neighborMax){
}

void PoissonSolverJacobiMPI::iterate(){
	m_err=0.0;
	//m_nze=0;
	// Send border data through non-blocking signals
	// Array for request handlers
	MPI_Request request[4];
	// Array of statuses
	MPI_Status status[4];
	int l(0);
	// Memory shift(and size)
	int shift(m_sizeZ*m_sizeY);
	if(m_neighborMin!=-1){
		MPI_Irecv(m_data,shift,_MPI_DATATYPE,m_neighborMin,0,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+shift,shift, _MPI_DATATYPE,m_neighborMin,1,MPI_COMM_WORLD,request+l++ );
	}
	if(m_neighborMax!=-1){
		MPI_Irecv(m_data+(m_sizeX-1)*shift,shift,_MPI_DATATYPE,m_neighborMax,1,MPI_COMM_WORLD,request+l++);
		MPI_Isend(m_data+(m_sizeX-2)*shift,shift ,_MPI_DATATYPE,m_neighborMax,0,MPI_COMM_WORLD,request+l++ );
	}
	// Meanwhile, iterate normally
	unsigned int b(2*shift+m_sizeZ);
	for(unsigned int i(2);i<m_sizeX-2;i++){
		for(unsigned int j(1);j<m_sizeY-1;j++){
			for(unsigned int k(1);k<m_sizeZ-1;k++){
				m_tmp[b+k]=m_j_ax*(m_data[-shift+b+k]+m_data[shift+b+k])+m_j_ay*(m_data[b-m_sizeZ+k]+m_data[b+m_sizeZ+k])+m_j_az*(m_data[b+k-1]+m_data[b+k+1])-m_j_af*m_func[b+k];
				m_err += pow(m_cX*(m_data[-shift+b+k]+m_data[shift+b+k]-2.0f*m_data[b+k])+m_cY*(m_data[b-m_sizeZ+k]+m_data[b+m_sizeZ+k]-2.0f*m_data[b+k])+m_cZ*(m_data[b+k-1]+m_data[b+k+1]-2.0f*m_data[b+k])-m_func[b+k],2);
			}
			b+=m_sizeZ;
		}
		b+=2*m_sizeZ;
	}
	// Wait for all the tasks
	MPI_Waitall(l,request,status);
	// Now that we have data, update boundaries
	b=m_sizeZ;
	unsigned int z((m_sizeX-2)*shift);
	for(unsigned int j(1);j<m_sizeY-1;j++){
		for(unsigned int k(1);k<m_sizeZ-1;k++){
			m_tmp[shift+b+k]=m_j_ax*(m_data[b+k]+m_data[2*shift+b+k])+m_j_ay*(m_data[shift+b-m_sizeZ+k]+m_data[shift+b+m_sizeZ+k])+m_j_az*(m_data[shift+b+k-1]+m_data[shift+b+k+1])-m_j_af*m_func[shift+b+k];
			m_tmp[z+b+k]=m_j_ax*(m_data[z-shift+b+k]+m_data[z+shift+b+k])+m_j_ay*(m_data[z+b-m_sizeZ+k]+m_data[z+b+m_sizeZ+k])+m_j_az*(m_data[z+b+k-1]+m_data[z+b+k+1])-m_j_af*m_func[z+b+k];
			m_err += pow(m_cX*(m_data[b+k]+m_data[2*shift+b+k]-2.0f*m_data[shift+b+k])+m_cY*(m_data[shift+b-m_sizeZ+k]+m_data[shift+b+m_sizeZ+k]-2.0f*m_data[shift+b+k])+m_cZ*(m_data[shift+b+k-1]+m_data[shift+b+k+1]-2.0f*m_data[shift+b+k])-m_func[shift+b+k],2)
			+pow(m_cX*(m_data[z-shift+b+k]+m_data[z+shift+b+k]-2.0f*m_data[z+b+k])+m_cY*(m_data[z+b-m_sizeZ+k]+m_data[z+b+m_sizeZ+k]-2.0f*m_data[z+b+k])+m_cZ*(m_data[z+b+k-1]+m_data[z+b+k+1]-2.0f*m_data[z+b+k])-m_func[z+b+k],2);
		}
		b+=m_sizeZ;
	}	// swap addresses to apply changes

	// Global variable for grouping
	P_REAL globalErr(0.0);
	// Collect data from all processes
	MPI_Allreduce ( &m_err, &globalErr, 1, _MPI_DATATYPE, MPI_SUM,MPI_COMM_WORLD );
	// Apply
	m_err = globalErr;

	P_REAL* swap(NULL);
	swap=m_data;
	m_data=m_tmp;
	m_tmp=swap;
}
